<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->enum('type_salary', array('D', 'F', 'C'))
                ->comment('D: Diario, F: Fijo, C: Comisión');

            $table->double('amount', 16, 2)
                ->default(0);

            $table->boolean('type_employee')
                ->comment('0: US-1099, 1: US-W2');

            $table->enum('payroll_type', array('S', 'Q'))
                ->comment('S: Semanal, Q: Quincenal');

            $table->string('percentage')
                ->default(0)
                ->comment('Solo para usuarios tipo salario es 0');

            $table->float('cost_pound')
                ->nullable();

            $table->float('cost_hour')
                ->nullable();

            $table->integer('assigned_by')
                ->comment('Id del usuario que hace la asignación');

            $table->string('location')
                ->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salaries');
    }
}
