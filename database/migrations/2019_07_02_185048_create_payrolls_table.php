<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payrolls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identifier');

            $table->boolean('payroll')
                ->default(0)
                ->comment('1: Semanal, 0: Quincenal');

            $table->date('begins');
            $table->date('finish');

            $table->boolean('state')
                ->comment('1: Abierta, 0: Cerrada');

            $table->unsignedInteger('user_id');

            $table->foreign('user_id')
                ->references('id')->on('users');
                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrolls');
    }
}
