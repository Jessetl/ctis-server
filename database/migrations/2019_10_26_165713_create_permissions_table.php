<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('moduleitem_id')->nullable();
            $table->foreign('moduleitem_id')->references('id')->on('module_item')->onDelete('cascade');
            $table->string('name')->unique();
            $table->string('display_name')->nullable()->comment('Human readable name for the Role. Not necessarily unique and optional. For example: “User Administrator”, “Project Owner”, “Widget Co. Employee”.');
            $table->text('description')->nullable()->comment('A more detailed explanation of what the Role does. Also, optional.');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
