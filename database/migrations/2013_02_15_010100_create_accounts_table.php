<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('address');
            $table->string('phone');
            $table->string('email')->nullable();
            $table->string('postal_code');
            $table->string('country_alphacode');
            $table->string('country_es')->nullable();
            $table->string('country_en')->nullable();
            $table->string('state');
            $table->string('city');
            $table->string('name_contact')->nullable();
            $table->string('email_contact')->nullable();
            $table->string('phone_contact')->nullable();
            $table->string('image')->nullable();
            $table->boolean('status')->default(1)->comment('1: Activo, 0: Inactivo');
            $table->integer('user_id')->comment('Id de usuario que crear o actualiza este registro');
            $table->boolean('haveAccess')->default(0);
            $table->string('location')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
