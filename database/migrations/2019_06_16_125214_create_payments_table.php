<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('invoice_id');
            $table->foreign('invoice_id')->references('id')->on('invoices');
            $table->boolean('type')->comment('1: Ingreso, 0: Egreso');
            $table->enum('method_payment', array('E', 'C', 'T', 'O'))->nullable()->comment('E: Efectivo, C: Cheque, T: Transferencia, O: Otro');
            $table->string('bank')->nullable();
            $table->double('amount', 16, 2)->comment('Total facturado menos el factoraje');
            $table->string('voucher')->nullable();
            $table->text('notes')->nullable();
            $table->unsignedInteger('user_id');
            $table->string('location')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
