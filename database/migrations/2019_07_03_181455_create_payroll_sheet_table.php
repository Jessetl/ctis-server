<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollSheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_sheet', function (Blueprint $table) {
            $table->increments('id');

            $table->string('identifier');
            $table->unsignedInteger('payroll_id');

            $table->foreign('payroll_id')
                ->references('id')
                ->on('payrolls')
                ->onDelete('Cascade');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->enum('payroll_type', array('S', 'Q'))
                ->nullable()
                ->comment('S: Semanal, Q: Quincenal');

            $table->integer('worked')
                ->default(0);

            $table->double('salary', 16, 2)
                ->default(0.00);

            $table->enum('type_salary', array('D', 'F', 'C'));
            $table->double('base_salary', 16, 2)
                ->default(0.00);

            $table->double('subtotal', 16, 2)
                ->default(0.00);

            $table->double('taxation', 16, 2)
                ->default(0)
                ->comment('Total de ingreso * porcentaje [tabla (users)]');

            $table->double('deductions', 16, 2)
                ->default(0);

            $table->double('assignments', 16, 2)
                ->default(0);

            $table->double('total', 16, 2)
                ->default(0.00)
                ->comment('Total de ingreso – total de deducciones – impuestos');

            $table->integer('status')
                ->default(1)
                ->comment('0: Cancelado, 1: Vigente, 2: Pagado');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_sheet');
    }
}
