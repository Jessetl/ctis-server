<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identifier')->unique()->comment('5 digitos, único');
            $table->unsignedInteger('account_id');
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->string('type');
            $table->string('name');
            $table->double('price', 16, 2);
            $table->double('cost', 16, 2);
            $table->boolean('type_load')->default(0)->comment('0: Manual, 1: Archivo');
            $table->integer('quantity');
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->integer('high')->nullable();
            $table->integer('weight')->nullable();
            $table->text('description');
            $table->integer('user_id');
            $table->string('location')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
