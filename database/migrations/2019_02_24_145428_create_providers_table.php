<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->text('address');
            $table->string('country_alphacode');
            $table->string('country_es')->nullable();
            $table->string('country_en')->nullable();
            $table->string('state');
            $table->string('city');
            $table->string('postal_code');
            $table->string('name_contact')->nullable();
            $table->string('email_contact')->nullable();
            $table->string('phone_contact')->nullable();
            $table->boolean('payment_method')->default(1)->comment('1: Crédito, 0: Contado');
            $table->string('credit_days');
            $table->text('notes')->nullable();
            $table->boolean('status')->default(1)->comment('1: Activo, 0: Inactivo');
            $table->integer('user_id');
            $table->string('location')->nullable();
            $table->unsignedInteger('account_id')->nullable();
            $table->boolean('haveAccess')->default(0);
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('Cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');
    }
}
