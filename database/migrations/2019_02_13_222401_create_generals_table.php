<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generals', function (Blueprint $table) {
            $table->increments('id', 5, 2);
            $table->double('percentage_trip', 5, 2);
            $table->double('factoring', 5, 2);
            $table->double('taxes', 5, 2);
            $table->double('commission', 5, 2);
            $table->double('card_payment', 5, 2);
            $table->double('insurance', 5, 2);
            $table->unsignedInteger('account_id');

            $table->foreign('account_id')
                ->references('id')
                ->on('accounts');

            $table->integer('user_id');
            $table->string('location');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generals');
    }
}
