<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsActivitiesToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->date('packaging_date')->after('work_date')->nullable();
            $table->date('delivery_date')->after('packaging_date')->nullable();
            $table->date('collect_date')->after('delivery_date')->nullable();
            $table->date('unpacking_date')->after('collect_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn([
                'packaging_date',
                'delivery_date',
                'collect_date',
                'unpacking_date',
            ]);
        });
    }
}
