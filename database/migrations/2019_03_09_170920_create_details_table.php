<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders')->onUpdate('cascade')->onDelete('cascade');
            $table->unsignedInteger('article_id')->comment('product_id o service_id');
            $table->enum('type', ['1', '2'])->comment('1: Producto, 2: Servicio');
            $table->text('description')->nullable();
            $table->integer('quantity')->comment('Cantidad del producto/servicio');
            $table->double('unitary_price', 16, 2)->comment('Precio unitario');
            $table->double('price', 16, 2)->comment('resultado de multiplicar cantidad * PU');
            $table->enum('load_type', ['regular', 'residencial', 'comercial'])->nullable();
            $table->double('height')->nullable();
            $table->double('high')->nullable();
            $table->double('width')->nullable();
            $table->double('measure_height')->nullable();
            $table->double('measure_high')->nullable();
            $table->double('measure_width')->nullable();
            $table->double('foot_cubic')->nullable()->comment('(height * high * width)/1728');
            $table->double('weight_vol')->nullable()->comment('(height * high * width)/139');
            $table->double('weight')->nullable();
            $table->double('re_weight')->nullable();
            $table->double('insurance', 16, 2)->default(0)->comment('precio * porcentaje del seguro indicado en la tabla comisiones');
            $table->double('taxation', 16, 2)->default(0)->comment('precio * porcentaje del impuesto indicado en la tabla comisiones');
            $table->double('subtotal', 16, 2)->comment('quantity * price_unitary');
            $table->double('total', 16, 2)->comment('suma de precio + seguro + impuesto');
            $table->text('content')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
}
