<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleSubitemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_subitem', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('moduleitem_id')->nullable();
            $table->foreign('moduleitem_id')->references('id')->on('module_item')->onDelete('cascade');
            $table->string('title');
            $table->string('path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_subitem');
    }
}
