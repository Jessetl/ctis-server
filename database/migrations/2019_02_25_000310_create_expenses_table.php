<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('type_service_id');
            $table->foreign('type_service_id')->references('id')->on('type_service');
            $table->unsignedInteger('provider_id');
            $table->foreign('provider_id')->references('id')->on('providers');
            $table->date('date');
            $table->text('description');
            $table->double('amount', 16, 2);
            $table->double('tax', 16, 2);
            $table->string('expense');
            $table->string('folio');
            $table->text('notes')->nullable();
            $table->string('voucher')->nullable();
            $table->boolean('status')->default(1);
            $table->unsignedInteger('user_id');
            $table->string('location')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
