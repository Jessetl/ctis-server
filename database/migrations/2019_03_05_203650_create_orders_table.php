<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identifier')->unique()->comment('5 digitos, único');
            $table->unsignedInteger('account_id');
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->unsignedInteger('customer_id')->nullable();
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->unsignedInteger('addre_id')->nullable();
            $table->foreign('addre_id')->references('id')->on('address');
            $table->string('ref')->nullable();
            $table->string('notes')->nullable();
            $table->string('work_ticket')->nullable();
            $table->string('number_order')->nullable();
            $table->boolean('invoice_to')->default(0)->comment('0: Cliente, 1: Cuenta');
            $table->text('instructions_especials')->nullable();
            $table->enum('type', ['O', 'C'])->comment('O: Orden de trabajo, C: Cotización')->nullable();
            $table->string('type_location')->nullable()->comment('L: Local, E: Estatal: I: Interstatal, CL: Crating Local, CI: Crating Inter Estatal');
            $table->double('price', 16, 2)->nullable();
            $table->date('work_date')->nullable();
            $table->double('estimated_value', 16, 2)->nullable();
            $table->time('work_begins')->nullable();
            $table->time('work_ends')->nullable();
            $table->integer('cub_foot')->nullable()->comment('Pie cubico');
            $table->integer('weight')->nullable();
            $table->integer('miles')->nullable();
            $table->boolean('type_payment')->comment('0: Efectivo, 1: Transferencia, 2: Tarjeta Debito, 3 Tarjeta Credito');
            $table->string('warehouse')->nullable()->comment('Numero de almacén');
            $table->integer('dist_weight')->nullable();
            $table->double('adjustment', 16, 2)->nullable()->comment('Ajuste por importe menor a 75');
            $table->integer('no_art')->comment('Nº. de servicios y/o productos agregados a la cotización');
            $table->double('subtotal', 16, 2)->comment('Suma del valor a cobrar por los productos y/o servicios antes de impuesto/seguro.');
            $table->double('taxation', 16, 2)->default(0)->comment('Suma de los impuestos');
            $table->double('insurance', 16, 2)->default(0)->comment('Suma del seguro');
            $table->double('descount', 16, 2)->default(0)->comment('Descuento a aplicar');
            $table->double('total', 16, 2)->comment('(subtotal + taxes + secure) - descount');
            $table->boolean('status')->default(1)->comment('1: Activa, 0: Cancelada, 2: Autorizada, 3: Procesada');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('location')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
