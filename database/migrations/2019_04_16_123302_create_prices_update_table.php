<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices_update', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('article_id')->comment('product_id o service_id');
            $table->unsignedInteger('detail_id');
            $table->foreign('detail_id')->references('id')->on('details')->onUpdate('cascade')->onDelete('cascade');
            $table->double('after_price', 16, 2)->comment('nuevo precio establecido.');
            $table->double('before_price', 16, 2)->comment('Precio en lista');
            $table->string('reason');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('location')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices_update');
    }
}
