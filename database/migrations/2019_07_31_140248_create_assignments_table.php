<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('user_id')
                ->comment('Usuario que tiene la asignación');
            
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            
            $table->unsignedInteger('order_id')
                ->comment('Orden asignada');
            
            $table->foreign('order_id')
                ->references('id')
                ->on('orders');
            
            $table->unsignedInteger('activity_id')
                ->comment('Actividad Asignada');
            
            $table->foreign('activity_id')
                ->references('id')
                ->on('activities');

            $table->date('work_date');

            $table->enum('type_payment', array('P', 'D', 'H', 'C', 'F'));
            
            $table->time('work_begins')->nullable();

            $table->time('work_ends')->nullable();

            $table->unsignedInteger('assigned_by')
                ->comment('Id del usuario que hace la asignación');
            
            $table->boolean('status')
                ->default(0)
                ->comment('Referencia a la orden para saber si esta terminada');

            $table->foreign('assigned_by')
                ->references('id')
                ->on('users');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
