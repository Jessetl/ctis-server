<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollConceptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_concepts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('identifier');
            $table->unsignedInteger('sheet_id');

            $table->foreign('sheet_id')
                ->references('id')
                ->on('payroll_sheet');

            $table->unsignedInteger('concept_id');

            $table->foreign('concept_id')
                ->references('id')
                ->on('payroll_sheet');

            $table->double('amount', 16, 2)
                ->default(0.00);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_concepts');
    }
}
