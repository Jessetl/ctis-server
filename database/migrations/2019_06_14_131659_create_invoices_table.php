<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('Cascade');
            $table->double('factoring', 16, 2)->comment('Porcentaje indicado en las configuraciones de la empresa.');
            $table->double('total_factoring', 16, 2)->nullable();
            $table->double('paid', 16, 2)->nullable();
            $table->enum('type', array('FC', 'FT'))->comment('FC: Factura, FT: Factoraje.');
            $table->boolean('status')->default(0);
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('location')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
