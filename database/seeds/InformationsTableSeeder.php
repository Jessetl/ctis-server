<?php

use Illuminate\Database\Seeder;

class InformationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Information::create([
            'name' => 'CML Group',
            'address' => 'Lorem ipsum',
            'country_alphacode' => 'US',
            'country_es' => 'Estados Unidos',
            'country_en' => 'United States',
            'state' => 'California',
            'city' => 'Los Angeles',
            'postal_code' => 2123,
            'phone' => 111111111,
            'email' => 'exports@ctis.com',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
