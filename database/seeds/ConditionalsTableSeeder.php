<?php

use Illuminate\Database\Seeder;

class ConditionalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Loop 1
        App\Conditional::create([
        	'id' => 1,
            'pound_id' => 1,
            'min' => 1000,
            'max' => 8000,
            'amount' => 112,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Conditional::create([
        	'id' => 2,
            'pound_id' => 1,
            'min' => null,
            'max' => null,
            'amount' => 0,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Conditional::create([
        	'id' => 3,
            'pound_id' => 1,
            'min' => null,
            'max' => null,
            'amount' => 0,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Conditional::create([
        	'id' => 4,
            'pound_id' => 1,
            'min' => null,
            'max' => null,
            'amount' => 0,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Conditional::create([
        	'id' => 5,
            'pound_id' => 1,
            'min' => null,
            'max' => null,
            'amount' => 0,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        // Loop 2

        App\Conditional::create([
        	'id' => 6,
            'pound_id' => 2,
            'min' => 1000,
            'max' => 10000,
            'amount' => 112,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Conditional::create([
        	'id' => 7,
            'pound_id' => 2,
            'min' => null,
            'max' => null,
            'amount' => 0,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Conditional::create([
        	'id' => 8,
            'pound_id' => 2,
            'min' => null,
            'max' => null,
            'amount' => 0,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Conditional::create([
        	'id' => 9,
            'pound_id' => 2,
            'min' => null,
            'max' => null,
            'amount' => 0,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Conditional::create([
        	'id' => 10,
            'pound_id' => 2,
            'min' => null,
            'max' => null,
            'amount' => 0,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        // Loop 3

        App\Conditional::create([
        	'id' => 11,
            'pound_id' => 3,
            'min' => 1000,
            'max' => 8000,
            'amount' => 80,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Conditional::create([
        	'id' => 12,
            'pound_id' => 3,
            'min' => null,
            'max' => null,
            'amount' => 0,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Conditional::create([
        	'id' => 13,
            'pound_id' => 3,
            'min' => null,
            'max' => null,
            'amount' => 0,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Conditional::create([
        	'id' => 14,
            'pound_id' => 3,
            'min' => null,
            'max' => null,
            'amount' => 0,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Conditional::create([
        	'id' => 15,
            'pound_id' => 3,
            'min' => null,
            'max' => null,
            'amount' => 0,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

    }
}
