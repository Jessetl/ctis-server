<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
class ModulesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		App\Module::insert([
			['id' => 1, 'title' => 'menu.config.title', 'icon' => 'fa fa-cog', 'active' => false, 'type' => 'dropdown', 'path' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 2, 'title' => 'menu.catalogs.title', 'icon' => 'fa fa-truck-moving', 'active' => false, 'type' => 'dropdown', 'path' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 3, 'title' => 'menu.expenses.title', 'icon' => 'fa fa-money-bill-alt', 'active' => false, 'type' => 'dropdown', 'path' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 4, 'title' => 'menu.orders.title', 'icon' => 'fa fa-shopping-cart', 'active' => false, 'type' => 'dropdown', 'path' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 5, 'title' => 'menu.payments.title', 'icon' => 'fa fa-chart-line', 'active' => false, 'type' => 'dropdown', 'path' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 6, 'title' => 'menu.payrolls.title', 'icon' => 'fa fa-calculator', 'active' => false, 'type' => 'dropdown', 'path' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 7, 'title' => 'menu.statements.title', 'icon' => 'fa fa-piggy-bank', 'active' => false, 'type' => 'dropdown', 'path' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 8, 'title' => 'menu.reports.title', 'icon' => 'fa fa-file-alt', 'active' => false, 'type' => 'dropdown', 'path' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 9, 'title' => 'menu.results.title', 'icon' => 'fa fa-chart-line', 'active' => false, 'type' => 'dropdown', 'path' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
		]);
	}
}
