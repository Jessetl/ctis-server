<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(PositionsTableSeeder::class);
        $this->call(PoundsTableSeeder::class);
        $this->call(ConditionalsTableSeeder::class);
        $this->call(ActivitiesTableSeeder::class);
        $this->call(DimensionTableSeeder::class);
        $this->call(TypeServiceTableSeeder::class);
        $this->call(ConceptsTableSeeder::class);
        $this->call(AccountsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(InformationsTableSeeder::class);
        $this->call(GeneralsTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(ProviderServiceTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(AddressTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ModuleItemTableSeeder::class);
        $this->call(ModuleSubitemTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);
    }
}
