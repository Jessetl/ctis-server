<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class ModuleSubitemTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		App\ModuleSubitem::insert([
			// Configuración
			['id' => 1, 'moduleitem_id' => 6, 'title' => 'Agregar', 'path' => '/catalogs/users/form', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 2, 'moduleitem_id' => 6, 'title' => 'Generales', 'path' => '/config/general', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 3, 'moduleitem_id' => 6, 'title' => 'Asignar Sueldo', 'path' => '/catalogs/users/assignments', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
		]);
	}
}
