<?php

use Illuminate\Database\Seeder;

class DimensionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Dimension::create([
            'term' => 1,
            'min' => 0,
            'max' => 3,
            'value' => 6,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Dimension::create([
            'term' => 2,
            'min' => 4,
            'max' => 5,
            'value' => 8,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Dimension::create([
            'term' => 3,
            'min' => 6,
            'max' => 7,
            'value' => 10,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Dimension::create([
            'term' => 4,
            'min' => 8,
            'max' => 9,
            'value' => 12,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Dimension::create([
            'term' => 5,
            'min' => 12,
            'max' => null,
            'value' => 4,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
