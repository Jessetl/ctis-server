<?php

use Illuminate\Database\Seeder;

class GeneralsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('generals')->insert([
            'percentage_trip' => 10,
            'factoring' => 12.4,
            'taxes' => 10,
            'commission' => 12.5,
            'card_payment' => 9,
            'insurance' => 8,
            'account_id' => 1,
            'user_id' => 1,
            'location' => 'Maracay',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
