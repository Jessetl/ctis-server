<?php

use Illuminate\Database\Seeder;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->insert([
            ['name' => 'Administrador'],
            ['name' => 'Gerencia'],
            ['name' => 'Contabilidad'],
            ['name' => 'Recursos Humanos'],
            ['name' => 'Supervisor'],
            ['name' => 'Técnico'],
            ['name' => 'Conductor'],
            ['name' => 'Ayudante'],
        ]);
    }
}
