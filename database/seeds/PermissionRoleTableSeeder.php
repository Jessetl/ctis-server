<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class PermissionRoleTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// Modules for administrator
		for($i = 1; $i <= 9; $i++) {
			for($j = 1; $j <= 2; $j++) {
				DB::table('role_module')->insert(['role_id' => $j, 'module_id' => $i, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
			}
		}
		// for administrator
		for($i = 1; $i <= 61; $i++) {
			DB::table('permission_role')->insert(['permission_id' => $i, 'role_id' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
		}
		// for manager
		for($i = 1; $i <= 61; $i++) {
			DB::table('permission_role')->insert(['permission_id' => $i, 'role_id' => 2, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
		}
	}
}
