<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class ModuleItemTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		App\ModuleItem::insert([
			// Configuración
			['id' => 1, 'module_id' => 1, 'title' => 'menu.config.menu.information', 'path' => '/config/info', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 2, 'module_id' => 1, 'title' => 'menu.config.menu.general', 'path' => '/config/general', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 3, 'module_id' => 1, 'title' => 'menu.config.menu.profile', 'path' => '/config/profiles', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Catálogos
			['id' => 4, 'module_id' => 2, 'title' => 'menu.catalogs.menu.service', 'path' => '/catalogs/services', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 5, 'module_id' => 2, 'title' => 'menu.catalogs.menu.product', 'path' => '/catalogs/products', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 6, 'module_id' => 2, 'title' => 'menu.catalogs.menu.user.title', 'path' => '/catalogs/users', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 7, 'module_id' => 2, 'title' => 'menu.catalogs.menu.customer', 'path' => '/catalogs/customers', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 8, 'module_id' => 2, 'title' => 'menu.catalogs.menu.account', 'path' => '/catalogs/accounts', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 9, 'module_id' => 2, 'title' => 'menu.catalogs.menu.provider', 'path' => '/catalogs/providers', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Gastos
			['id' => 10, 'module_id' => 3, 'title' => 'menu.expenses.menu.add', 'path' => '/expenses/form', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 11, 'module_id' => 3, 'title' => 'menu.expenses.menu.consult', 'path' => '/expenses', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Orden
			['id' => 12, 'module_id' => 4, 'title' => 'menu.orders.menu.add', 'path' => '/orders/form', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 13, 'module_id' => 4, 'title' => 'menu.orders.menu.consult', 'path' => '/orders', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 14, 'module_id' => 4, 'title' => 'menu.orders.menu.assign', 'path' => '/orders/assignments', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 15, 'module_id' => 4, 'title' => 'menu.orders.menu.movement', 'path' => '/orders/movements', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Pago
			['id' => 16, 'module_id' => 5, 'title' => 'menu.payments.menu.add', 'path' => '', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 17, 'module_id' => 5, 'title' => 'menu.payments.menu.consult', 'path' => '', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Nominas
			['id' => 18, 'module_id' => 6, 'title' => 'menu.payrolls.menu.assistance', 'path' => '/payrolls/assists', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 19, 'module_id' => 6, 'title' => 'menu.payrolls.menu.calculate', 'path' => '/payrolls/generate', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 20, 'module_id' => 6, 'title' => 'menu.payrolls.menu.consult', 'path' => '/payrolls', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 21, 'module_id' => 6, 'title' => 'menu.payrolls.menu.receipt', 'path' => '', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 22, 'module_id' => 6, 'title' => 'menu.payrolls.menu.accumulated', 'path' => '', 'type' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Estado de Cuenta
			['id' => 23, 'module_id' => 7, 'title' => 'menu.statements.menu.customer', 'path' => '', 'type' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 24, 'module_id' => 7, 'title' => 'menu.statements.menu.account', 'path' => '', 'type' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 25, 'module_id' => 7, 'title' => 'menu.statements.menu.provider', 'path' => '', 'type' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Reportes
			['id' => 26, 'module_id' => 8, 'title' => 'menu.reports.menu.order', 'path' => '', 'type' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 27, 'module_id' => 8, 'title' => 'menu.reports.menu.catalogue', 'path' => '', 'type' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 28, 'module_id' => 8, 'title' => 'menu.reports.menu.movement', 'path' => '', 'type' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 29, 'module_id' => 8, 'title' => 'menu.reports.menu.income', 'path' => '', 'type' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 30, 'module_id' => 8, 'title' => 'menu.reports.menu.egress', 'path' => '', 'type' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 31, 'module_id' => 8, 'title' => 'menu.reports.menu.commission', 'path' => '', 'type' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 32, 'module_id' => 8, 'title' => 'menu.reports.menu.factoring', 'path' => '', 'type' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Statement of income
			['id' => 33, 'module_id' => 9, 'title' => 'menu.results.menu.profitloss', 'path' => '', 'type' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
		]);
	}
}
