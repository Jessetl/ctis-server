<?php

use Illuminate\Database\Seeder;

class ConceptsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('concepts')->insert([
        	// ----------------------------------- ASIGNACIONES
            [
            	'type' => 'A', 
            	'notion' => 'Sueldo',
            	'visibility' => 0
            ],
            [
            	'type' => 'A', 
            	'notion' => 'Comisión',
            	'visibility' => 0
            ],
            [
            	'type' => 'A', 
            	'notion' => 'Vacaciones',
            	'visibility' => 1
            ],
            [
            	'type' => 'A', 
            	'notion' => 'Aguinaldos',
            	'visibility' => 1
            ],
            [
            	'type' => 'A', 
            	'notion' => 'Tiempo Extra',
            	'visibility' => 1
            ],
            [
            	'type' => 'A', 
            	'notion' => 'Otros ingresos',
            	'visibility' => 1
            ],
            // -------------------------------------- DEDUCCIONES
            [
            	'type' => 'D', 
            	'notion' => 'Aseguranza',
            	'visibility' => 1
            ],
            [
            	'type' => 'D', 
            	'notion' => 'Ausencias',
            	'visibility' => 1
            ],
            [
            	'type' => 'D', 
            	'notion' => 'Otras deducciones',
            	'visibility' => 1
            ]
        ]);
    }
}
