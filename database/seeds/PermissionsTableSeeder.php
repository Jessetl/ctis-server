<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
class PermissionsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		App\Permission::insert([
			// Configuration - Information
			['id' => 1, 'moduleitem_id' => 1, 'name' => 'read-information', 'display_name' => 'Read Information', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 2, 'moduleitem_id' => 1, 'name' => 'write-information', 'display_name' => 'Write Information', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 3, 'moduleitem_id' => 1, 'name' => 'rewrite-information', 'display_name' => 'Rewrite Information', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Configuration - General
			['id' => 4, 'moduleitem_id' => 2, 'name' => 'read-general', 'display_name' => 'Read General', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 5, 'moduleitem_id' => 2, 'name' => 'write-general', 'display_name' => 'Write General', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 6, 'moduleitem_id' => 2, 'name' => 'rewrite-general', 'display_name' => 'Rewrite General', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Configuration - Profile
			['id' => 7, 'moduleitem_id' => 3, 'name' => 'read-profile', 'display_name' => 'Read Profile', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 8, 'moduleitem_id' => 3, 'name' => 'write-profile', 'display_name' => 'Write Profile', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 9, 'moduleitem_id' => 3, 'name' => 'rewrite-profile', 'display_name' => 'Rewrite Profile', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Catalogs - Services
			['id' => 10, 'moduleitem_id' => 4, 'name' => 'read-service', 'display_name' => 'Read Service', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 11, 'moduleitem_id' => 4, 'name' => 'write-service', 'display_name' => 'Write Service', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 12, 'moduleitem_id' => 4, 'name' => 'rewrite-service', 'display_name' => 'Rewrite Service', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Catalogs - Products
			['id' => 13, 'moduleitem_id' => 5, 'name' => 'read-product', 'display_name' => 'Read Product', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 14, 'moduleitem_id' => 5, 'name' => 'write-product', 'display_name' => 'Write Product', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 15, 'moduleitem_id' => 5, 'name' => 'rewrite-product', 'display_name' => 'Rewrite Product', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Catalogs Users
			['id' => 16, 'moduleitem_id' => 6, 'name' => 'read-user', 'display_name' => 'Read User', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 17, 'moduleitem_id' => 6, 'name' => 'write-user', 'display_name' => 'Write User', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 18, 'moduleitem_id' => 6, 'name' => 'rewrite-user', 'display_name' => 'Rewrite User', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Catalogs Customers
			['id' => 19, 'moduleitem_id' => 7, 'name' => 'read-customer', 'display_name' => 'Read Customer', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 20, 'moduleitem_id' => 7, 'name' => 'write-customer', 'display_name' => 'Write Customer', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 21, 'moduleitem_id' => 7, 'name' => 'rewrite-customer', 'display_name' => 'Rewrite Customer', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Catalogs Accounts
			['id' => 22, 'moduleitem_id' => 8, 'name' => 'read-account', 'display_name' => 'Read Account', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 23, 'moduleitem_id' => 8, 'name' => 'write-account', 'display_name' => 'Write Account', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 24, 'moduleitem_id' => 8, 'name' => 'rewrite-account', 'display_name' => 'Rewrite Account', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Catalogs Providers
			['id' => 25, 'moduleitem_id' => 9, 'name' => 'read-provider', 'display_name' => 'Read Provider', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 26, 'moduleitem_id' => 9, 'name' => 'write-provider', 'display_name' => 'Write Provider', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 27, 'moduleitem_id' => 9, 'name' => 'rewrite-provider', 'display_name' => 'Rewrite Provider', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Expenses
			['id' => 28, 'moduleitem_id' => 10, 'name' => 'write-expense', 'display_name' => 'Write Expense', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 29, 'moduleitem_id' => 11, 'name' => 'read-expense', 'display_name' => 'Read Expense', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 30, 'moduleitem_id' => 11, 'name' => 'rewrite-expense', 'display_name' => 'Rewrite Expense', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Orders
			['id' => 31, 'moduleitem_id' => 12, 'name' => 'write-order', 'display_name' => 'Write Order', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 32, 'moduleitem_id' => 13, 'name' => 'read-order', 'display_name' => 'Read Order', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Order - Assignment
			['id' => 33, 'moduleitem_id' => 14, 'name' => 'read-assignment', 'display_name' => 'Read Assignment', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 34, 'moduleitem_id' => 14, 'name' => 'write-assignment', 'display_name' => 'Write Assignment', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 35, 'moduleitem_id' => 14, 'name' => 'rewrite-assignment', 'display_name' => 'Rewrite Assignment', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Order - Movements
			['id' => 36, 'moduleitem_id' => 15, 'name' => 'read-movement', 'display_name' => 'Read Movement', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 37, 'moduleitem_id' => 15, 'name' => 'write-movement', 'display_name' => 'Write Movement', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 38, 'moduleitem_id' => 15, 'name' => 'rewrite-movement', 'display_name' => 'Rewrite Movement', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Payments
			['id' => 39, 'moduleitem_id' => 16, 'name' => 'write-payment', 'display_name' => 'Write Payment', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 40, 'moduleitem_id' => 17, 'name' => 'read-payment', 'display_name' => 'Read Payment', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 41, 'moduleitem_id' => 17, 'name' => 'rewrite-payment', 'display_name' => 'Rewrite Payment', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Payrrolls - Assistance
			['id' => 42, 'moduleitem_id' => 18, 'name' => 'write-assistance', 'display_name' => 'Write Assistance', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 43, 'moduleitem_id' => 18, 'name' => 'read-assistance', 'display_name' => 'Read Assistance', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 44, 'moduleitem_id' => 18, 'name' => 'rewrite-assistance', 'display_name' => 'Rewrite Assistance', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Payrrolls - Payrolls
			['id' => 45, 'moduleitem_id' => 19, 'name' => 'write-payroll', 'display_name' => 'Write Payroll', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 46, 'moduleitem_id' => 19, 'name' => 'rewrite-payroll', 'display_name' => 'Rewrite Payroll', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Payrrolls - Payrolls
			['id' => 47, 'moduleitem_id' => 20, 'name' => 'read-payroll', 'display_name' => 'Write Payroll', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Payrolls - Receipts
			['id' => 48, 'moduleitem_id' => 21, 'name' => 'read-receipt', 'display_name' => 'Read Receipt', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Payrolls - Accumulated
			['id' => 49, 'moduleitem_id' => 22, 'name' => 'read-accumulated', 'display_name' => 'Read Accumulated', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 50, 'moduleitem_id' => 22, 'name' => 'write-accumulated', 'display_name' => 'Write Accumulated', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Account statements
			['id' => 51, 'moduleitem_id' => 23, 'name' => 'print-statements-customer', 'display_name' => 'Print Statements / Customer', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 52, 'moduleitem_id' => 24, 'name' => 'print-statements-account', 'display_name' => 'Print Statements / Account', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 53, 'moduleitem_id' => 25, 'name' => 'print-statements-provider', 'display_name' => 'Print Statements / Provider', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Reports
			['id' => 54, 'moduleitem_id' => 26, 'name' => 'print-statements-order', 'display_name' => 'Print Statements / Order', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 55, 'moduleitem_id' => 27, 'name' => 'print-statements-catalog', 'display_name' => 'Print Statements / Catalog', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 56, 'moduleitem_id' => 28, 'name' => 'print-statements-movement', 'display_name' => 'Print Statements / Movement', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 57, 'moduleitem_id' => 29, 'name' => 'print-statements-income', 'display_name' => 'Print Statements / Income', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 58, 'moduleitem_id' => 30, 'name' => 'print-statements-expense', 'display_name' => 'Print Statements / Expense', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 59, 'moduleitem_id' => 31, 'name' => 'print-statements-commission', 'display_name' => 'Print Statements / Commission', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			['id' => 60, 'moduleitem_id' => 32, 'name' => 'print-statements-factoring', 'display_name' => 'Print Statements / Factoring', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
			// Account statements
			['id' => 61, 'moduleitem_id' => 33, 'name' => 'print-statements-profitloss', 'display_name' => 'Print Statements / (Profit / Loss)', 'description' => NULL, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
		]);
	}
}
