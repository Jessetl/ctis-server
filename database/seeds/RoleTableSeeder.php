<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class RoleTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		App\Role::insert([
			['id' => 1, 'name' => 'Administrador', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()], // 1
			['id' => 2,'name' => 'Gerencia', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()], // 2
			['id' => 3,'name' => 'Contabilidad', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()], // 3
			['id' => 4,'name' => 'Recursos Humanos', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()], // 4
			['id' => 5,'name' => 'Atención a clientes', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()], // 5
			['id' => 6,'name' => 'Despacho', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()], // 6
			['id' => 7,'name' => 'Técnico', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()], // 7
			['id' => 8,'name' => 'Conductor', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()], // 8
			['id' => 9,'name' => 'Supervisor', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()] // 9
		]);
	}
}
