<?php

use Illuminate\Database\Seeder;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Account::create([
            'name' => 'CML Group',
            'address' => 'Lorem ipsum',
            'country_alphacode' => 'US',
            'country_es' => 'Estados Unidos',
            'country_en' => 'United States',
            'state' => 'California',
            'city' => 'Los Angeles',
            'postal_code' => '90001',
            'phone' => '1235454545',
            'email' => 'ventas@cmlgroup.com',
            'user_id' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        //factory(App\Account::class, 29)->create();
    }
}
