<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		App\User::create([
			'name' => 'Admin',
			'email' => 'admin@ctis.com',
			'email_verified_at' => now(),
			'password' => bcrypt('123456'),
			'address' => 'Calle x cruce con z casa #30',
			'phone' => '02432610944',
			'mobile' => '04243358379',
			'country_alphacode' => 'VE',
			'country_es' => 'Venezuela',
			'country_en' => 'Venezuela',
			'state' => 'Aragua',
			'city' => 'Maracay',
			'position_id' => 1,
			'property' => 'C',
			'postal_code' => '2123',
			'user' => 'ctis',
			'account_id' => 1,
			'user_id' => 1,
			'status' => 1,
			'created_at' => \Carbon\Carbon::now(),
			'updated_at' => \Carbon\Carbon::now()
		]);

		DB::table('role_user')->insert(['user_id' => 1, 'role_id' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);

		//factory(App\User::class, 29)->create();
	}
}
