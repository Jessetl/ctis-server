<?php

use Illuminate\Database\Seeder;

class TypeServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_service')->insert([
            ['name' => 'Insumo'],
            ['name' => 'Servicio'],
            ['name' => 'Transporte'],
            ['name' => 'Asesoría Fiscal'],
            ['name' => 'Prestación de Personal'],
            ['name' => 'Arrendamiento'],
            ['name' => 'Seguros'],
            ['name' => 'Impuestos'],
            ['name' => 'Otros']
        ]);
    }
}
