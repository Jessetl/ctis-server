<?php

use Illuminate\Database\Seeder;

class PoundsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Pound::create([
        	'id' => 1,
            'position_id' => 5,
            'pound_cost' => 0.011,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Pound::create([
        	'id' => 2,
            'position_id' => 6,
            'pound_cost' => 0.011,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Pound::create([
        	'id' => 3,
            'position_id' => 7,
            'pound_cost' => 0.011,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        App\Pound::create([
        	'id' => 4,
            'position_id' => 8,
            'pound_cost' => 0.01,
            'user_id' => 1,
            'location' => null,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
