<?php

use Faker\Generator as Faker;

$factory->define(App\Provider::class, function (Faker $faker) {
   
    return [
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'address' => $faker->address,
        'country_alphacode' => 'US',
        'country_es' => 'Estados Unidos',
        'country_en' => 'United States',
        'state' => $faker->state,
        'city' => $faker->city,
        'postal_code' => $faker->postcode,
        'name_contact' => $faker->name,
        'email_contact' => $faker->unique()->safeEmail,
        'phone_contact' => $faker->phoneNumber,
        'payment_method' =>  $faker->randomElement($array = array (1, 0)),
        'credit_days' => $faker->randomNumber(2),
        'notes' => $faker->text(200),
        'status' => $faker->randomElement($array = array (1, 0)),
        'user_id' => App\User::all()->random()->id,
        'account_id' => App\Account::all()->random()->id,
        'haveAccess' => $faker->randomElement($array = array (1, 0))
    ];

});
