<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    
    \Bezhanov\Faker\ProviderCollectionHelper::addAllProvidersTo($faker);

    return [
        'identifier' => strtoupper(str_random(5)),
        'account_id' => 1,//App\Account::all()->random()->id,
        'type' => $faker->department(1),
        'name' => $faker->productName,
        'price' => $faker->randomNumber(2),
        'cost' => $faker->randomNumber(2),
        'type_load' => 0,
        'quantity' => $faker->randomNumber(1),
        'width' => $faker->randomNumber(2),
        'height' => $faker->randomNumber(2),
        'high' => $faker->randomNumber(2),
        'weight' => $faker->randomNumber(2),
        'description' => $faker->text(25),
        'user_id' => App\User::all()->random()->id
    ];

});
