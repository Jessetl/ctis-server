<?php

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'address' => $faker->address,
        'phone' => NULL,
        'country_alphacode' => 'US',
        'country_es' => 'Estados Unidos',
        'country_en' => 'United States',
        'mobile' => NULL,
        'postal_code' => $faker->postcode,
        'user' => $faker->userName,
        'password' => bcrypt('123456'), // secret
        'state' => $faker->state,
        'city' => $faker->city,
        'position_id' => App\Position::get()->except([1])->random()->id,
        'property' => $faker->randomElement($array = array ('C', 'P', 'E')),
        'account_id' => 1,
        'user_id' => 1,
        'status' => 1,
        'created_at' =>  \Carbon\Carbon::now(),
        'updated_at' =>  \Carbon\Carbon::now()
    ];

});
