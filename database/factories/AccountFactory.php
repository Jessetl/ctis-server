<?php

use Faker\Generator as Faker;

$factory->define(App\Account::class, function (Faker $faker) {
    
    return [
        'name' => $faker->company,
        'address' => $faker->address,
        'phone' => 1111111111,
        'email' => $faker->unique()->safeEmail,
        'postal_code' => $faker->postcode,
        'country_alphacode' => 'US',
        'country_es' => 'Estados Unidos',
        'country_en' => 'United States',
        'state' => $faker->state,
        'city' => $faker->city,
        'name_contact' => $faker->name,
        'email_contact' => $faker->unique()->safeEmail,
        'phone_contact' => $faker->phoneNumber,
        'status' => $faker->randomElement($array = array (1, 0)),
        'user_id' => App\User::all()->random()->id,
        'haveAccess' => $faker->randomElement($array = array (1, 0)),
        'created_at' =>  \Carbon\Carbon::now(),
        'updated_at' =>  \Carbon\Carbon::now()
    ];

});
