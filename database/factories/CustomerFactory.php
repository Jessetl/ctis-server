<?php

use Faker\Generator as Faker;

$factory->define(App\Customer::class, function (Faker $faker) {
    
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'country_alphacode' => 'US',
        'country_es' => 'Estados Unidos',
        'country_en' => 'United States',
        'state' => $faker->state,
        'city' => $faker->city,
        'postal_code' => $faker->postcode,
        'status' => $faker->randomElement($array = array (1, 0)),
        'account_id' => 1, //App\Account::all()->random()->id,
        'user_id' => App\User::all()->random()->id,
        'haveAccess' => $faker->randomElement($array = array (1, 0))
    ];

});
