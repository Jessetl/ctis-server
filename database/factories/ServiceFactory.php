<?php

use Faker\Generator as Faker;

$factory->define(App\Service::class, function (Faker $faker) {

    return [
        'identifier' => strtoupper(str_random(5)),
        'type' => $faker->jobTitle,
        'name' => $faker->catchPhrase,
        'description' => $faker->text(25),
        'account_id' => 1,//App\Account::all()->random()->id,
        'price' => $faker->randomNumber(2),
        'cost' => $faker->randomNumber(2),
        'load' => $faker->randomNumber(2),
        'max_weight' => $faker->randomNumber(2),
        'type_load' => 1,
        'created_at' =>  \Carbon\Carbon::now(),
        'updated_at' =>  \Carbon\Carbon::now()
    ];  

});
