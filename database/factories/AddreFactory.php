<?php

use Faker\Generator as Faker;

$factory->define(App\Addre::class, function (Faker $faker) {

    return [
        'customer_id' => App\Customer::all()->random()->id,
        'address' => $faker->address,
        'country_alphacode' => 'US',
        'country_es' => 'Estados Unidos',
        'country_en' => 'United States',
        'state' => $faker->state,
        'city' => $faker->city,
        'postal_code' => $faker->postcode,
    ];

});
