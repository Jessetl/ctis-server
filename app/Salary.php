<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    protected $table = "salaries";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'type_salary', 'amount', 'type_employee', 'payroll_type', 'percentage', 
        'cost_pound', 'cost_hour', 'type_payment', 'assigned_by', 'location'
    ];

    /**
     * Obtain the user's relationship
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
