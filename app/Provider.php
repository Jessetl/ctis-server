<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $table = "providers";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'email', 'address', 'country_alphacode', 'country_es', 'country_en',
        'state', 'city', 'postal_code', 'name_contact', 'email_contact', 'phone_contact', 'payment_method',
        'credit_days', 'notes', 'status', 'user_id', 'haveAccess', 'location', 'haveAccess', 'account_id'
    ];

    public function account()
    {
        return $this->belongsTo('App\Provider', 'account_id');
    }

    public function services()
    {
        return $this->belongsToMany('App\TypeService', 'provider_has_services', 'provider_id', 'type_service_id');
    }
}
