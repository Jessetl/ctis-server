<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleSubitem extends Model
{
    protected $table = "module_subitem";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'moduleitem_id', 'title', 'path'
    ];

    /**
     * Obtain the user profile relationship
     *
     * @return mixed
     */
    public function submenus()
    {
        return $this->belongsTo('App\SubMenu', 'submenu_id');
    }
}
