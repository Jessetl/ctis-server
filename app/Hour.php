<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hour extends Model
{
    use SoftDeletes;

    protected $table = "hours";
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'assists_id', 'work_begins', 'work_ends', 'hours'
    ];

    public function assists()
    {
        return $this->belongsTo('App\Assistance', 'assists_id', 'id');
    }
}
