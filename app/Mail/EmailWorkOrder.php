<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailWorkOrder extends Mailable
{
    use SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->order->invoice_to ? $this->order->customer->email : $this->order->addre->customer->email;

        return $this->markdown('emails.workorder')
            ->subject('Nueva notificación de orden de trabajo')
            ->from($email, 'CML Support Groups')
            ->with('order', $this->order);
    }
}
