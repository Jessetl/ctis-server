<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Quote extends Mailable
{
  use SerializesModels;

  public $order;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($order)
  {
    $this->order = $order;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->markdown('emails.quote')
      ->subject('Nueva notificación de cotización')
      ->from('operations@cmlexports.com', 'CML Support Groups')
      ->with('order', $this->order);
  }
}