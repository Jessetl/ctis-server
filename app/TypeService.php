<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeService extends Model
{
    protected $table = "type_service";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function providers()
    {
        return $this->belongsToMany('App\Provider', 'provider_has_services', 'type_service_id', 'provider_id');
    }
}
