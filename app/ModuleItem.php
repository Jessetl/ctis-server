<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleItem extends Model
{
	protected $table = "module_item";

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'module_id', 'title', 'path', 'type'
	];

	/**
	 * Obtain the user profile relationship
	 *
	 * @return mixed
	 */
	public function menus()
	{
		return $this->belongsTo('App\Menu', 'menu_id');
	}

	/**
	 * Obtain the user profile relationship
	 *
	 * @return mixed
	 */
	public function subitems()
	{
		return $this->hasMany('App\ModuleSubitem', 'moduleitem_id', 'id');
	}
}
