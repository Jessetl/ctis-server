<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;

    protected $table = "accounts";
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'phone', 'email', 'postal_code', 'country_alphacode', 'country_es',
        'country_en', 'state', 'city', 'name_contact', 'email_contact', 'phone_contact', 'image',
        'status', 'trip_charge', 'min_amount', 'user_id', 'haveAccess', 'location'
    ];

    public function commission()
    {
        return $this->hasOne('App\Commission', 'account_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function customers()
    {
        return $this->hasMany('App\Customer', 'account_id', 'id');
    }
}
