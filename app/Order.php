<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sign', 'account_id', 'customer_id', 'addre_id', 'ref', 'gbl', 'equipment', 'notes', 'work_ticket', 'number_order', 'invoice_to', 'instructions_especials',
        'type', 'type_location', 'signing', 'signatory', 'date_signatory', 'price', 'work_date', 'packaging_date', 'delivery_date', 'collect_date', 'unpacking_date', 'estimated_value', 'work_begins', 'work_ends', 'cub_foot',
        'weight', 'miles', 'type_payment', 'warehouse', 'dist_weight', 'adjustment', 'no_art', 'subtotal', 'taxation', 'insurance', 'trip_charge',
        'com_payment', 'card_payment', 'descount', 'total', 'status', 'reactivation', 'user_id', 'haveTravelTime', 'location',
    ];

    public function account()
    {
        return $this->belongsTo('App\Account', 'account_id', 'id');
    }

    public function addre()
    {
        return $this->belongsTo('App\Addre', 'addre_id', 'id');
    }

    public function details()
    {
        return $this->hasMany('App\Detail', 'order_id', 'id');
    }

    public function file()
    {
        return $this->hasMany('App\File', 'order_id', 'id');
    }

    public function invoice()
    {
        return $this->hasOne('App\Invoice', 'order_id', 'id');
    }

    public function activity()
    {
        return $this->belongsToMany('App\Activity', 'order_activity', 'order_id', 'activity_id')->withPivot('id', 'work_date')->withTimestamps();
    }

    public function activities()
    {
        return $this->hasMany('App\OrderActivity', 'order_id', 'id');
    }

    public function assignments()
    {
        return $this->hasMany('App\Assignment', 'order_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setDateSignatoryAttribute($date)
    {
        $this->attributes['date_signatory'] = Carbon::parse($date)->format('Y-m-d');
    }
}
