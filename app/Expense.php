<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expense extends Model
{
    use SoftDeletes;
    
    protected $table = "expenses";
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_service_id', 'provider_id', 'date', 'description', 'amount', 'tax',
        'expense', 'folio', 'notes', 'voucher', 'status', 'user_id', 'location'
    ];

    public function service()
    {
        return $this->belongsTo('App\TypeService', 'type_service_id');
    }

    public function provider()
    {
        return $this->belongsTo('App\Provider', 'provider_id');
    }
}
