<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderActivity extends Model
{
   	protected $table = "order_activity";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'activity_id', 'work_date'
    ];

    /**
     * Obtain the order
     *
     * @return mixed
     */
    public function order ()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }

    /**
     * Obtain the activity
     *
     * @return mixed
     */
    public function activity ()
    {
        return $this->belongsTo('App\Activity', 'activity_id');
    }

    /**
     * Obtain the activity
     *
     * @return mixed
     */
    public function assignments ()
    {
    	return $this->hasMany('App\Assignment', 'oract_id');
    }
}
