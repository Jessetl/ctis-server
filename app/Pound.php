<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pound extends Model
{
    protected $table = "pounds";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'position_id', 'pound_cost', 'user_id', 'location'
    ];

    public function position ()
    {
        return $this->belongsTo('App\Position', 'position_id');
    }

    public function conditionals ()
    {
    	return $this->hasMany('App\Conditional', 'pound_id', 'id');
    }
}
