<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
  protected $table = "modules";

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'title', 'icon', 'active', 'type', 'path'
  ];

  /**
   * Obtain the user profile relationship
   *
   * @return mixed
   */
  public function items()
  {
    return $this->hasMany('App\ModuleItem', 'module_id', 'id')->where('type', 1);
  }
}
