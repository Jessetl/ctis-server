<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = "payments";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'invoice_id', 'type', 'method_payment', 'bank', 'day_payment', 'amount',
        'voucher', 'notes', 'user_id', 'location'
    ];
}
