<?php

namespace App\Http\Traits;

use PDF;
use Storage;
use Carbon\Carbon;

trait PDFs
{
	public function createFileQuotation($order)
	{
		$folder = 'public/' . $order->account->name . '/' . 'COTIZACION' . '/' . Carbon::now()->format('Y') . '/' . Carbon::now()->format('m');
		$folderNotPublic = $order->account->name . '/' . 'COTIZACION' . '/' . Carbon::now()->format('Y') . '/' . Carbon::now()->format('m');
		$fileName = Carbon::now()->format('Y') . Carbon::now()->format('m') . Carbon::now()->format('d') . '_' . $order->identifier;

		$pdf = PDF::loadView('files.quote', ['order' => $order]);

		if (!Storage::exists($folder)) Storage::makeDirectory($folder);

		$path = storage_path('app/' . $folder);

		$documentRoot = $path . '/' . $fileName . '.pdf';

		$pdf->save($documentRoot);

		return env('APP_URL') . 'storage/' . $folderNotPublic . '/' . $fileName . '.pdf';
	}

	public function createFileWorkOrder($order)
	{
		$folder = 'public/' . $order->account->name . '/' . 'ORDEN DE TRABAJO' . '/' . Carbon::now()->format('Y') . '/' . Carbon::now()->format('m');
		$folderNotPublic = $order->account->name . '/' . 'ORDEN DE TRABAJO' . '/' . Carbon::now()->format('Y') . '/' . Carbon::now()->format('m');
		$fileName = Carbon::now()->format('Y') . Carbon::now()->format('m') . Carbon::now()->format('d') . '_' . $order->identifier;

		$pdf = PDF::loadView('files.workorder', ['order' => $order]);

		if (!Storage::exists($folder)) Storage::makeDirectory($folder);

		$path = storage_path('app/' . $folder);

		$documentRoot = $path . '/' . $fileName . '.pdf';

		$pdf->save($documentRoot);

		return env('APP_URL') . 'storage/' . $folderNotPublic . '/' . $fileName . '.pdf';
	}

	public function createFileInvoice($order)
	{
		$folder = 'public/' . $order->account->name . '/' . 'FACTURA' . '/' . Carbon::now()->format('Y') . '/' . Carbon::now()->format('m');
		$folderNotPublic = $order->account->name . '/' . 'FACTURA' . '/' . Carbon::now()->format('Y') . '/' . Carbon::now()->format('m');
		$fileName = Carbon::now()->format('Y') . Carbon::now()->format('m') . Carbon::now()->format('d') . '_' . $order->identifier;

		$pdf = PDF::loadView('files.invoice', ['order' => $order]);

		if (!Storage::exists($folder)) Storage::makeDirectory($folder);

		$path = storage_path('app/' . $folder);

		$documentRoot = $path . '/' . $fileName . '.pdf';

		$pdf->save($documentRoot);

		return env('APP_URL') . 'storage/' . $folderNotPublic . '/' . $fileName . '.pdf';
	}

	public function createSignatureWorkOrder($order, $img)
	{
		$image = $img;  // your base64 encoded
		$image = str_replace('data:image/png;base64,', '', $image);
		$image = str_replace(' ', '+', $image);

		$folder = '/public/' . $order->account->name . '/' . 'FIRMA' . '/' . Carbon::now()->format('Y') . '/' . Carbon::now()->format('m');
		$folderNotPublic = $order->account->name . '/' . 'FIRMA' . '/' . Carbon::now()->format('Y') . '/' . Carbon::now()->format('m');
		$fileName = Carbon::now()->format('Y') . Carbon::now()->format('m') . Carbon::now()->format('d') . '_' . $order->identifier . '.png';

		if (!Storage::exists($folder)) Storage::makeDirectory($folder);

		\File::put(storage_path() . '/app/public/' . $folderNotPublic . '/' . $fileName, base64_decode($image));

		return env('APP_URL') . 'storage/' . $folderNotPublic . '/' . $fileName;
	}

	public function createFileFactoring($order)
	{
		$folder = 'public/' . $order->account->name . '/' . 'FACTORING' . '/' . Carbon::now()->format('Y') . '/' . Carbon::now()->format('m');
		$folderNotPublic = $order->account->name . '/' . 'FACTORING' . '/' . Carbon::now()->format('Y') . '/' . Carbon::now()->format('m');
		$fileName = Carbon::now()->format('Y') . Carbon::now()->format('m') . Carbon::now()->format('d') . '_' . $order->identifier;

		$pdf = PDF::loadView('files.factoring', ['order' => $order]);

		if (!Storage::exists($folder)) Storage::makeDirectory($folder);

		$path = storage_path('app/' . $folder);

		$documentRoot = $path . '/' . $fileName . '.pdf';

		$pdf->save($documentRoot);

		return env('APP_URL') . 'storage/' . $folderNotPublic . '/' . $fileName . '.pdf';
	}
}
