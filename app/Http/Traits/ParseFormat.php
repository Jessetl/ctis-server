<?php

namespace App\Http\Traits;

use Carbon\Carbon;

trait ParseFormat
{
    public function parseFormat($date)
    {
        return Carbon::parse($date)->format('m/d/Y');
    }
}
