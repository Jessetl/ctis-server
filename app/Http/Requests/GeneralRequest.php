<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GeneralRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'percentage_trip' => 'required',
            'factoring' => 'required',
            'taxes' => 'required',
            'commission' => 'required',
            'card_payment' => 'required',
            'insurance' => 'required',
            'notas' => 'required',
            'user_id' => 'required',
        ];
    }
}
