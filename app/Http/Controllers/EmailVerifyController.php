<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Carbon\Carbon;

class EmailVerifyController extends Controller
{
    public function verify($string)
    {
        $user = User::where('encrypted_confirmation', $string)->firstOrFail();
        $user->status = true;
        $user->email_verified_at = Carbon::now();
        $user->encrypted_confirmation = null;
        $user->save();

        return view('verify.confirmation', [
            'user' => $user
        ]);
    }
}
