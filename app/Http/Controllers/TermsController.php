<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TermsDimension as Terms;

class TermsController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function getIncraseWidth($width)
    {
        $terms  = Terms::all();

        foreach ($terms as $key => $term) {
            if (($width >= $term->min) && ($width <= $term->max)) return ['value' => $term->value, 'type' => 'REP'];
            elseif ((is_null($term->max)) && ($width >= $term->min)) return ['value' => $term->value, 'type' => 'INC'];
        }
    }
}
