<?php

namespace App\Http\Controllers\Orders;

use App\Assignment;
use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AssignmentsController extends Controller
{
    public function getOrderWithAssignments(Request $request)
    {
        $me = $request->user();

        if (($me['position_id'] === 1)) {
            $orders = Order::with([

                'file',
                'account',
                'invoice',
                'customer',
                'addre.customer',
                'activities.activity',
                'activities' => function ($query) {
                    $query->orderBy('work_date', 'ASC');
                },
                'assignments',
                'assignments.user',
                'assignments.activity',
                'assignments.user.position',

            ])->where([

                ['type', 'O'],

            ])->orderBy('id', 'DESC')->get();

        } else {

            $orders = Order::with([

                'file',
                'account',
                'invoice',
                'customer',
                'addre.customer',
                'activities.activity',
                'activities' => function ($query) {
                    $query->orderBy('work_date', 'ASC');
                },
                'assignments',
                'assignments.user',
                'assignments.activity',
                'assignments.user.position',

            ])->where([

                ['account_id', $me['account_id']], ['type', 'O'],

            ])->orderBy('id', 'DESC')->get();
        }

        return $orders;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormToAssign(array $data)
    {
        return Validator::make($data, [
            'user_id' => ['required', 'numeric'],
            'order_id' => ['required', 'numeric'],
            'type_payment' => ['required', 'in:P,H,D,C,F'],
        ]);
    }

    public function assignUser(Request $request)
    {
        $this->ValidatorFormToAssign($request->all())->validate();

        $me = $request->user();

        if (($request->has('activities'))) {

            foreach ($request->activities as $key => $activity) {

                $assignment = new Assignment();
                $assignment->assigned_by = $me['id'];
                $assignment->activity_id = $activity['activity_id'];
                $assignment->work_date = $activity['work_date'];
                $assignment->fill($request->except(['activity_id', 'work_date', 'assigned_by']))->save();
            }
        }

        $order = Order::with([

            'file',
            'account',
            'invoice',
            'customer',
            'addre.customer',
            'activities.activity',
            'activities' => function ($query) {
                $query->orderBy('work_date', 'ASC');
            },
            'assignments',
            'assignments.user',
            'assignments.activity',
            'assignments.user.position',

        ])->findOrFail($request->order_id);

        if ($request->has('equipment')) {
            $order->equipment = $request->equipment;
            $order->save();
        }

        return $order;
    }

    public function assingTimesToAssignment(Request $request)
    {
        $assignment = Assignment::findOrFail($request->id);
        $assignment->fill($request->all())->save();

        return $assignment;
    }

    public function getAssignmentsWithUser()
    {
        $assignaments = Assignment::with([

            'user',
            'order',
            'activity',
            'user.position',
            'order.customer',
            'order.addre.customer',

        ])->get();

        return $assignaments;
    }

    public function deleteAssignedUser($id)
    {
        $assignment = Assignment::findOrFail($id);
        $assignment->delete();

        return $assignment;
    }

    public function getByDate($from, $to)
    {
        $query = Assignment::with([
            'order',
            'user',
            'activity',
            'user.position',
        ]);

        if ($from != 'null' && $to != 'null') {

            $query->whereBetween('work_date', [$from, $to]);

        } else if ($from != 'null') {

            $query->whereDate('work_date', '>=', $from);

        } else if ($to != 'null') {

            $query->whereDate('work_date', '<=', $to);
        }

        return $query->get();
    }
}
