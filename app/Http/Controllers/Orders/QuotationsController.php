<?php

namespace App\Http\Controllers\Orders;

use App\File;
use App\Http\Controllers\Controller;
use App\Http\Traits\ParseFormat;
use App\Http\Traits\PDFs;
use App\Mail\EmailQuote;
use App\Order;
use Illuminate\Http\Request;
use Mail;

define('CANCELED', 0);
define('PENDING', 1);
define('AUTHORIZED', 2);
define('PROCESS', 3);

class QuotationsController extends Controller
{
    use PDFs, ParseFormat;

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function getQuotations(Request $request)
    {
        $me = $request->user();

        if (($me['position_id'] === 1)) {
            $orders = Order::with([
                'file',
                'account',
                'customer',
                'activities',
                'addre.customer',
            ])->orderBy('id', 'DESC')->get();
        } else {

            $orders = Order::with([
                'file',
                'account',
                'customer',
                'activities',
                'addre.customer',
            ])->where([
                ['account_id', $me['account_id']], ['type', 'C'],
            ])->orderBy('id', 'DESC')->get();
        }

        return $orders;
    }

    public function getByDate($from, $to)
    {
        $query = Order::with([
            'user',
            'account',
            'customer',
            'addre.customer',
            'assignaments.position',
        ]);

        if ($from != 'null' && $to != 'null') {
            $query->whereBetween('work_date', [$from, $to]);
        } else if ($from != 'null') {

            $query->whereDate('work_date', '>=', $from);
        } else if ($to != 'null') {

            $query->whereDate('work_date', '<=', $to);
        }

        return $query->get();
    }

    public function sendEmail(Request $request)
    {
        $order = Order::with([
            'user',
            'file',
            'account',
            'details',
            'customer',
            'addre.customer',
            'details.product',
            'details.service',
        ])->findOrFail($request->Id);

        $file = $order->file[0];
        $order->file = $file;

        $email = $order->invoice_to ? $order->customer->email : $order->addre->customer->email;

        Mail::to($email)->send(new EmailQuote($order));

        return $order;
    }

    public function authorizeQuote($id)
    {
        $order = Order::with([
            'user',
            'file',
            'activity',
            'account',
            'details',
            'customer',
            'addre.customer',
            'details.product',
            'details.service',
        ])->findOrFail($id);

        $order->type = 'O';
        $order->status = AUTHORIZED;
        $order->save();

        if ($order->activity->contains('pivot.activity_id', 1)) {

            $activity_delivery = $order->activity->where('pivot.activity_id', 1);
            $count = $activity_delivery->count();

            if ($count > 1) {

                $delivery = $activity_delivery->get();
                $order->activity_delivery = $this->parseFormat($delivery[0]->pivot->work_date) . ' - ' . $this->parseFormat($delivery[$count - 1]->pivot->work_date);

            } else {

                $order->activity_delivery = $this->parseFormat($activity_delivery->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 2)) {

            $activity_packaging_date = $order->activity->where('pivot.activity_id', 2);
            $count = $activity_packaging_date->count();

            if ($count > 1) {

                $packaging_date = $activity_packaging_date->get();
                $order->activity_packaging_date = $this->parseFormat($packaging_date[0]->pivot->work_date) . ' - ' . $this->parseFormat($packaging_date[$count - 1]->pivot->work_date);

            } else {

                $order->activity_packaging_date = $this->parseFormat($activity_packaging_date->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 3)) {

            $activity_unpack_range = $order->activity->where('pivot.activity_id', 3);
            $count = $activity_unpack_range->count();

            if ($count > 1) {

                $unpack_range = $activity_unpack_range->get();
                $order->activity_unpack_range = $this->parseFormat($unpack_range[0]->pivot->work_date) . ' - ' . $this->parseFormat($unpack_range[$count - 1]->pivot->work_date);

            } else {

                $order->activity_unpack_range = $this->parseFormat($activity_unpack_range->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 4)) {

            $activity_load_range = $order->activity->where('pivot.activity_id', 4);
            $count = $activity_load_range->count();

            if ($count > 1) {

                $load_range = $activity_load_range->get();
                $order->activity_load_range = $this->parseFormat($load_range[0]->pivot->work_date) . ' - ' . $this->parseFormat($load_range[$count - 1]->pivot->work_date);

            } else {

                $order->activity_load_range = $this->parseFormat($activity_load_range->first()->pivot->work_date);
            }
        }

        if (count($order->activity) > 0) {
            $order->services = $order->activity->unique('pivot.activity_id')->sortBy('pivot.id')->implode('name', ', ');
        }

        $path = $this->createFileWorkOrder($order);

        if ($find = $order->file()->where('type', 'workorder')->first()) {
            $file = File::findOrFail($find['id']);
        } else {
            $file = new File();
        }

        $file->order_id = $order['id'];
        $file->url_document = $path;
        $file->type = 'workorder';
        $file->save();

        return $file;
    }
}
