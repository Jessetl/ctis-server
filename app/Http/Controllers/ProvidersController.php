<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use Mail;
use App\User;
use App\Provider;
use App\TypeService;
use App\Mail\EmailConfirmation;

class ProvidersController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = Provider::with('account')->orderBy('id', 'DESC')->get();

        return $providers;
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getProviders(Request $request, $id)
    {
        $me = $request->user();
    
        if ( $me['account_id'] !== 1 ) {

            $providers = TypeService::with(['providers' => function ($query) use ($me) {
                $query->where([ ['status', 1], ['account_id', $me['account_id']] ]);
            }])->findOrFail($id);
            
        } else {
            
            $providers = TypeService::with(['providers' => function ($query) {
                $query->where('status', 1);
            }])->findOrFail($id);
        }

        return $providers;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormStore(array $data)
    {
        return Validator::make($data, [
            'name'              => ['required'],
            'phone'             => ['required', 'numeric', 'digits_between:7,10'],
            'email'             => ['required', 'string', 'email', 'unique:users'],
            'address'           => ['required'],
            'country_alphacode' => ['required'],
            'state'             => ['required'],
            'city'              => ['required'],
            'postal_code'       => ['required', 'numeric'],
            'name_contact'      => ['required'],
            'email_contact'     => ['required', 'string', 'email'],
            'phone_contact'     => ['required', 'numeric', 'digits_between:7,10'],
            'payment_method'    => ['required', 'boolean'],
            'credit_days'       => ['nullable', 'numeric'],
            'notes'             => ['nullable'],
            'user'              => ['nullable', 'alpha_dash', 'unique:users'],
            'password'          => ['nullable', 'string', 'min:6'],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->ValidatorFormStore($request->all())->validate();

        $me = $request->user();
        $haveAccess = false;

        if ( !empty($request['user']) OR !empty($request['password']) ) {
            
            $request->password = bcrypt($request['password']);

            $user = new User();
            $user->user_id = $me['id'];
            $user->account_id = $me['account_id'];
            $user->property = 'P';
            $user->password = bcrypt($request['password']);
            $user->status = true;
            $user->encrypted_confirmation = str_random(25);
            $user->fill($request->except(['user_id', 'account_id', 'property', 'password', 'status', 'encrypted_confirmation']))->save();

            Mail::to($user->email)->send(new EmailConfirmation($user));

            $haveAccess = true;
        }

        $provider = new Provider();
        $provider->user_id = $me['id'];
        $provider->account_id = $me['account_id'];
        $provider->haveAccess = $haveAccess;
        $provider->fill($request->except(['user_id', 'account_id', 'haveAccess']))->save();

        $provider->services()->sync($request['services']);

        return $provider;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $provider = Provider::with('services')->findOrFail($id);
        $provider->user = $provider->haveAccess ? User::where('email', $provider->email)->first() : null;

        return $provider;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormUpdate(array $data, $id)
    {
        return Validator::make($data, [
            'name'              => ['required'],
            'phone'             => ['required', 'numeric', 'digits_between:7,10'],
            'email'             => ['required', 'string', 'email', Rule::unique('providers', 'email')->ignore($id)],
            'address'           => ['required'],
            'country_alphacode' => ['required'],
            'state'             => ['required'],
            'city'              => ['required'],
            'postal_code'       => ['required', 'numeric'],
            'name_contact'      => ['required'],
            'email_contact'     => ['required', 'string', 'email'],
            'phone_contact'     => ['required', 'numeric', 'digits_between:7,10'],
            'payment_method'    => ['required', 'boolean'],
            'credit_days'       => ['nullable', 'numeric'],
            'notes'             => ['nullable']
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorFormUserUpdate(array $data, $id)
    {
        return Validator::make($data, [
            'user'     => ['required', 'alpha_dash', Rule::unique('users', 'user')->ignore($id)],
            'password' => ['nullable', 'string', 'min:6']
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->ValidatorFormUpdate($request->all(), $id)->validate();

        $me = $request->user();
        $provider = Provider::findOrFail($id);
        $userFromProvider = $provider->haveAccess ? User::where('email', $provider->email)->first() : null;

        $haveAccess = $provider->haveAccess;

        if ( (!empty($request['user']) OR !empty($request['password'])) AND (!$provider['haveAccess']) ) {
            
            $request->password = bcrypt($request['password']);

            $user = new User();
            $user->user_id = $me['id'];
            $user->account_id = $me['account_id'];
            $user->property = 'P';
            $user->password = bcrypt($request['password']);
            $user->status = true;
            $user->encrypted_confirmation = str_random(25);
            $user->fill($request->except(['user_id', 'account_id', 'property', 'password', 'status', 'encrypted_confirmation']))->save();

            Mail::to($user->email)->send(new EmailConfirmation($user));

            $haveAccess = true;

        } elseif ( (!empty($request['password'])) AND ($provider['haveAccess']) ) {

            $this->validatorFormUserUpdate($request->all(), $userFromProvider['id'])->validate();

            $user = $userFromProvider;
            $user->user = $request['user'];
            $user->password = bcrypt($request['password']);
            $user->save();

        } elseif ( (!empty($request['user'])) AND ($provider['haveAccess']) ) {

            $this->validatorFormUserUpdate($request->all(), $userFromProvider['id'])->validate();
            
            $user = $userFromProvider;
            $user->user = $request['user'];
            $user->save();
        }

        $provider->user_id = $me['id'];
        $provider->account_id = $me['account_id'];
        $provider->haveAccess = $haveAccess;
        $provider->fill($request->except(['user_id', 'account_id', 'haveAccess']))->save();

        return $provider;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeStatusProvider($id)
    {
        $provider = Provider::with('account')->findOrFail($id);
        $provider->status = ( ($provider->status) ) ? false : true;
        $provider->save();
        
        return $provider;
    }
}
