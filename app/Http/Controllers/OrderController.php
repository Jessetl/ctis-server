<?php

namespace App\Http\Controllers;

use App\Account;
use App\General;
use App\Http\Traits\ParseFormat;
use App\Http\Traits\PDFs;
use App\Order;
use App\OrderActivity;
use Illuminate\Http\Request;
use PDF;

define('CANCELED', 0);
define('PENDING', 1);
define('AUTHORIZED', 2);
define('PROCESS', 3);

class OrderController extends Controller
{
    use PDFs, ParseFormat;

    public function __construct()
    {
        // $this->middleware('jwt.auth');
    }

    public function viewFile($id)
    {
        $order = Order::with([
            'user',
            'file',
            'activity',
            'assignments',
            'assignments.activity',
            'assignments.user',
            'assignments.user.position',
            'account',
            'details',
            'customer',
            'addre.customer',
            'details.product',
            'details.service',
        ])->findOrFail($id);

        $order->assignments = $order->assignments->unique('user_id');

        if ($order->activity->contains('pivot.activity_id', 1)) {

            $activity_delivery = $order->activity->where('pivot.activity_id', 1);
            $count = $activity_delivery->count();

            if ($count > 1) {

                $delivery = $activity_delivery->get();
                $order->activity_delivery = $this->parseFormat($delivery[0]->pivot->work_date) . ' - ' . $this->parseFormat($delivery[$count - 1]->pivot->work_date);

            } else {

                $order->activity_delivery = $this->parseFormat($activity_delivery->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 2)) {

            $activity_packaging_date = $order->activity->where('pivot.activity_id', 2);
            $count = $activity_packaging_date->count();

            if ($count > 1) {

                $packaging_date = $activity_packaging_date->get();
                $order->activity_packaging_date = $this->parseFormat($packaging_date[0]->pivot->work_date) . ' - ' . $this->parseFormat($packaging_date[$count - 1]->pivot->work_date);

            } else {

                $order->activity_packaging_date = $this->parseFormat($activity_packaging_date->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 3)) {

            $activity_unpack_range = $order->activity->where('pivot.activity_id', 3);
            $count = $activity_unpack_range->count();

            if ($count > 1) {

                $unpack_range = $activity_unpack_range->get();
                $order->activity_unpack_range = $this->parseFormat($unpack_range[0]->pivot->work_date) . ' - ' . $this->parseFormat($unpack_range[$count - 1]->pivot->work_date);

            } else {

                $order->activity_unpack_range = $this->parseFormat($activity_unpack_range->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 4)) {

            $activity_load_range = $order->activity->where('pivot.activity_id', 4);
            $count = $activity_load_range->count();

            if ($count > 1) {

                $load_range = $activity_load_range->get();
                $order->activity_load_range = $this->parseFormat($load_range[0]->pivot->work_date) . ' - ' . $this->parseFormat($load_range[$count - 1]->pivot->work_date);

            } else {

                $order->activity_load_range = $this->parseFormat($activity_load_range->first()->pivot->work_date);
            }
        }

        if (count($order->activity) > 0) {
            $order->services = $order->activity->unique('pivot.activity_id')->sortBy('pivot.id')->implode('name', ', ');
        }

        $pdf = PDF::loadView('files.workorder', ['order' => $order])->stream('file.pdf');

        return $pdf;
    }

    public function index()
    {
        $orders = Order::with(['account', 'customer', 'file', 'addre.customer', 'assignaments.position'])->where('type', 'C')->orderBy('id', 'DESC')->get();

        return $orders;
    }

    public function getId()
    {
        $identifier = strtoupper(str_random(10));

        return $identifier;
    }

    public function store(Request $request)
    {
        $me = $request->user();
        $general = General::findOrFail(1);

        $percentage_card = $general->card_payment / 100;

        $account = Account::findOrFail($request->account_id);

        $order = new Order();
        $order->identifier = $this->getId();
        $order->user_id = $me['id'];
        $order->type = 'C';
        $order->no_art = 0;
        $order->trip_charge = $account->trip_charge;
        $order->com_payment = ($request['type_payment'] === "2" || $request['type_payment'] === "3") ? $request['total'] * $percentage_card : 0;
        $order->card_payment = ($request['type_payment'] === "2" || $request['type_payment'] === "3") ? $percentage_card : 0;
        $order->total = $request['total'] + $order->com_payment;

        $order->fill($request->except([
            'identifier',
            'type',
            'no_art',
            'trip_charge',
            'com_payment',
            'card_payment',
            'total',
            'user_id',
        ]))->save();

        if (($request->has('activities'))) {

            foreach ($request->activities as $key => $activity) {
                $relationship = new OrderActivity(['activity_id' => $activity['id'], 'work_date' => $activity['work_date']]);
                $order->activities()->save($relationship);
            }
        }

        return $order;
    }

    public function edit($id)
    {
        $order = Order::with([
            'account',
            'customer',
            'addre.customer',
            'activity' => function ($query) {
                $query->orderBy('order_activity.id');
            },
            'details',
        ])->findOrFail($id);

        return $order;
    }

    public function update(Request $request, $id)
    {
        $me = $request->user();
        $general = General::findOrFail(1);

        $percentage_card = $general->card_payment / 100;

        $account = Account::findOrFail($request->account_id);

        $order = Order::findOrFail($id);
        $order->user_id = $me->id;
        $order->type = ($order->status == 2 || $order->status == 3) ? 'O' : 'C';
        $order->no_art = 0;
        $order->trip_charge = $account->trip_charge;
        $order->com_payment = ($request['type_payment'] === 2 || $request['type_payment'] === 3) ? $request['total'] * $percentage_card : 0;
        $order->card_payment = ($request['type_payment'] === 2 || $request['type_payment'] === 3) ? $percentage_card : 0;
        $order->total = $request['total'] + $order->com_payment;

        $order->fill($request->except(['type', 'date_signatory', 'no_art', 'trip_charge', 'com_payment', 'card_payment', 'total', 'user_id']))->save();

        if (($request->has('activities'))) {

            $order->activities()->delete();

            foreach ($request->activities as $key => $activity) {

                $relationship = new OrderActivity(['activity_id' => $activity['id'], 'work_date' => $activity['work_date']]);

                $order->activities()->save($relationship);
            }
        }

        return $order;
    }

    public function activated()
    {
        $orders = Order::where('status', AUTHORIZED)
            ->with('assignaments.position', 'addre.customer', 'user')
            ->get();

        return $orders;
    }

    public function getByDate(Request $request, $from, $to)
    {
        $me = $request->user();

        if (($me['position_id'] === 1)) {

            $query = Order::with([

                'file',
                'account',
                'invoice',
                'customer',
                'addre.customer',
                'activities.activity',
                'activities' => function ($query) {
                    $query->orderBy('work_date', 'ASC');
                },
                'assignments',
                'assignments.user',
                'assignments.activity',
                'assignments.user.position',

            ])->where([

                ['type', 'O'], ['status', AUTHORIZED],

            ]);
        } else {

            $query = Order::with([

                'file',
                'account',
                'invoice',
                'customer',
                'addre.customer',
                'activities.activity',
                'activities' => function ($query) {
                    $query->orderBy('work_date', 'ASC');
                },
                'assignments',
                'assignments.user',
                'assignments.activity',
                'assignments.user.position',

            ])->where([

                ['account_id', $me['account_id']], ['type', 'O'], ['status', AUTHORIZED],

            ]);
        }

        if ($from != 'null' && $to != 'null') {
            $query->whereBetween('work_date', [$from, $to]);
        } else if ($from != 'null') {

            $query->whereDate('work_date', '>=', $from);
        } else if ($to != 'null') {

            $query->whereDate('work_date', '<=', $to);
        }

        return $query->get();
    }

    public function canceled($id)
    {
        $order = Order::findOrFail($id);
        $order->status = CANCELED;
        $order->save();

        return $order;
    }
}
