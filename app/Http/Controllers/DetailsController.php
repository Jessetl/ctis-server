<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Order;
use App\Detail;
use App\PriceUpdate;
use App\TermsDimension as Terms;

class DetailsController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function incraseWidth($width)
    {
        $terms  = Terms::get();

        foreach ($terms as $key => $term) {
            if ( ($width >= $term->min) && ($width <= $term->max) ) return $term->value;
                elseif ( ($term->min <= $width) && is_null($term->max) ) return $term->value;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $order = Order::findOrFail($id); 
        $me = $request->user();

        foreach ($request->all() as $key => $article) 
        {
            $detail = new Detail();
            $detail->order_id = $id;
            $detail->weight_vol = 0;
            $detail->fill(collect($article)->except(['order_id', 'weight_vol'])->toArray())->save();

            if ( ($article['change_price']) ) {
                
                $price = new PriceUpdate();
                $price->detail_id = $detail['id'];
                $price->after_price = $article['after'];
                $price->before_price = $article['before'];
                $price->user_id = $me->id;
                $price->fill(collect($article)->except(['detail_id', 'after_price', 'before_price', 'user_id'])->toArray())->save();
            }
        }

        $order->no_art = count($request->all());
        $order->save();
        
        return $order;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id); 
        $me = $request->user();
        
        foreach ($request->all() as $key => $article) 
        {
            if (isset($article['id'])) $detail = Detail::findOrFail($article['id']);
            else $detail = new Detail();

            $detail->order_id = $id;
            $detail->weight_vol = 0;
            $detail->fill(collect($article)->except(['order_id', 'weight_vol'])->toArray())->save();

            if ( isset($article['change_price']) ) 
            {
                if ( $article['change_price'] ) {

                    $price = new PriceUpdate();
                    $price->detail_id = $detail['id'];
                    $price->after_price = $article['after'];
                    $price->before_price = $article['before'];
                    $price->user_id = $me->id;
                    $price->fill(collect($article)->except(['detail_id', 'after_price', 'before_price', 'user_id'])->toArray())->save();
                }
            }
        }

        $order->no_art = count($request->all());
        $order->save();

        return $order;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $order = Order::with('details')->findOrFail($id); 
        
        foreach ($request->all() as $key => $article)
        {
            $detail = Detail::findOrFail($article['id'])->delete();
        }
        
        $order->no_art = count($order->details);
        $order->save();

        return $order;
    }
}
