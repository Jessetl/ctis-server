<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TypeService;
use App\Service;
use App\Product;

class TypeServicesController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type_services = TypeService::all();

        return $type_services;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTypeServices($account_id)
    {
        $types = Service::get()->groupBy('type');

        return $types;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTypeProducts($account_id)
    {
        $types = Product::get()->groupBy('type');

        return $types;
    }
}
