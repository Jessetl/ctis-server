<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Order;
use App\Assistance;
use App\OrderCommission;

class AssignmentsController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $me = $request->user();

        $users = User::with('position')->where([['property', 'E'], ['account_id', $me['account_id']]])->get();

        return $users;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return $user;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormUpdate(array $data)
    {
        return Validator::make($data, [
            'position_id'   => ['required'],
            'type_salary'   => ['required', 'in:D,F,C'],
            'type_employed' => ['required', 'boolean'],
            'percentage'    => ['nullable', 'numeric'],
            'payroll_type'  => ['required', 'in:S,Q'],
            'amount'        => ['required', 'numeric']
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorPercentageFormUpdate(array $data)
    {
        return Validator::make($data, [
            'percentage' => ['required']
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->ValidatorFormUpdate($request->all())->validate();
        if ( ($request->type_employed === 1) ) $this->ValidatorPercentageFormUpdate($request->all())->validate();

        $me = $request->user();

        $user = User::findOrFail($id);
        $user->user_id = $me['id'];
        $user->percentage = $request['type_employed'] ? $request['percentage'] : null;
        $user->salaryActive = true;
        $user->fill($request->except(['percentage', 'salaryActive']))->save();

        return $user;
    }

    public function assignmentOrder(Request $request, $id)
    {
        $order = Order::findOrFail($id);

        foreach ($request->all() as $key => $assignment) 
        {
            $base = $assignment['percentage'] / 100;

            $order_c = new OrderCommission();
            $order_c->percentage = $assignment['percentage'];
            $order_c->user_id = $assignment['id'];
            $order_c->order_id = $order['id'];
            $order_c->work_day = $order['work_date'];
            $order_c->subtotal = $order['subtotal'];
            $order_c->amount = $order['subtotal'] * $base;
            $order_c->save();
        }

        return $order;
    }
}
