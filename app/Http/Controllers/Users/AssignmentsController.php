<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\User;
use App\Salary;

class AssignmentsController extends Controller
{
    public function getUsersWithSalary(Request $request)
    {
    	$me = $request->user();

    	$users = Salary::with([

    		'user.position',
    		'user' => function ($query) use ($me) {
                $query->where([
                	['property', 'E'],
                	['account_id', $me['account_id']]
                ]);
            }

    	])->get();

    	return $users;
    }

    public function getSalaryWithUserEmployee($id)
    {
        $user = Salary::with([
            'user.position',
            'user'
        ])->findOrFail($id);

        return $user;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormStore(array $data)
    {
        return Validator::make($data, [
            'user_id'       => ['required'],
            'type_salary'   => ['required', 'in:D,F,C'],
            'type_employee' => ['required', 'boolean'],
            'payroll_type'  => ['required', 'in:S,Q'],
            'amount'        => ['required', 'numeric']
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorPercentageFormUpdate(array $data)
    {
        return Validator::make($data, [
            'percentage' => ['required', 'numeric']
        ]);
    }
    
    public function storeUserSalary(Request $request)
    {
        $this->ValidatorFormStore($request->all())->validate();

        if ( ($request->type_employed == 1) || ($request->type_salary == 'C') ) {
            $this->ValidatorPercentageFormUpdate($request->all())->validate();
        } 

        $me = $request->user();

        $salary = new Salary();
        $salary->assigned_by = $me['id'];
        $salary->percentage = $request['type_employed'] ? $request['percentage'] : 0;
        $salary->fill($request->except(['position_id', 'percentage', 'assigned_by']))->save();

        $user = User::findOrFail($request->user_id);
        $user->user_id = $me['id'];
        $user->salaryActive = true;
        $user->save();

        return $user;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormUpdate(array $data)
    {
        return Validator::make($data, [
            'user_id'       => ['required'],
            'type_salary'   => ['required', 'in:D,F,C'],
            'type_employee' => ['required', 'boolean'],
            'percentage'    => ['nullable', 'numeric'],
            'payroll_type'  => ['required', 'in:S,Q'],
            'amount'        => ['required', 'numeric']
        ]);
    }

    public function updateUserSalary(Request $request, $id)
    {
        $this->ValidatorFormUpdate($request->all())->validate();
        if ( ($request->type_employed == 1) || ($request->type_salary == 'C') ) $this->ValidatorPercentageFormUpdate($request->all())->validate();

        $me = $request->user();

        $salary = Salary::findOrFail($id);
        $salary->assigned_by = $me['id'];
        $salary->percentage = $request['type_employed'] ? $request['percentage'] : 0;
        $salary->fill($request->except(['position_id', 'percentage', 'assigned_by']))->save();

        return $salary;
    }
}
