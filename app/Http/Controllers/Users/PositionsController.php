<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Position;

class PositionsController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function getPositionsWithUsersEmployee(Request $request)
    {
        $me = $request->user();

        $users = Position::with([
            
            'users' => function ($query) use ($me) {
                $query->where([
                    ['property', 'E'],
                    ['account_id', $me['account_id']],
                    ['salaryActive', 0]
                ]);
            }

        ])->get()->except(1);

        return $users;
    }
}
