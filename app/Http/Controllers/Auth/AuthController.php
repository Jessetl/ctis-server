<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['login', 'refresh']]);
    }

    protected function ValidatorFormLogin(array $data)
    {
        return Validator::make($data, [
            'email'    => ['required', 'email', 'max:25'],
            'password' => ['required', 'string', 'min:6'],
            'language' => ['required', 'in:es,en']
        ]);
    }

    public function login(Request $request)
    {
        $this->ValidatorFormLogin($request->all())->validate();

        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => trans('auth.failed')], 401);
        }

        return $this->respondWithToken($token);
    }

    public function me()
    {
        return response()->json(auth()->user());
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'guard' => $this->guard()->user(),
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function guard()
    {
        return Auth::Guard();
    }
}
