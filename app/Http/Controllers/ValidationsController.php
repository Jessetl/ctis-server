<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ValidationsController extends Controller
{
	/**
	 * Create a new AuthController instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('jwt.auth');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validateFormOrder(array $data)
	{
		return Validator::make($data, [
			'account_id'             => ['required'],
			'invoice_to'             => ['required'],
			'type_location'          => ['nullable'],
			'instructions_especials' => ['nullable'],
			'price'                  => ['nullable'],
			'work_date'              => ['nullable'],
			'estimated_value'        => ['nullable'],
			'work_begins'            => ['nullable'],
			'work_ends'              => ['nullable'],
			'cub_foot'               => ['nullable'],
			'weight'                 => ['nullable'],
			'delivery_date'          => ['nullable'],
			'miles'                  => ['nullable'],
			'collect_date'           => ['nullable'],
			'type_payment'           => ['required'],
			'warehouse'              => ['nullable'],
			'dist_weight'            => ['nullable']
		]);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validateFormOrderLocal(array $data)
	{
		return Validator::make($data, [
			'work_ticket'  => ['nullable'],
			'number_order' => ['nullable']
		]);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validateFormOrderOther(array $data)
	{
		return Validator::make($data, [
			'ref'   => ['nullable'],
			'notes' => ['nullable']
		]);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validateFormAddress(array $data)
	{
		return Validator::make($data, [
			'addre_id' => ['required']
		]);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validateFormCustomer(array $data)
	{
		return Validator::make($data, [
			'customer_id' => ['required']
		]);
	}

	public function validateOrder(Request $request)
	{
		$this->validateFormOrder($request->all())->validate();

		if (($request['invoice_to'] === '0') || ($request['invoice_to'] === 0)) $this->validateFormAddress($request->all())->validate();
		else $this->validateFormCustomer($request->all())->validate();

		if (($request['type_location'] === 'L')) $this->validateFormOrderLocal($request->all())->validate();
		else $this->validateFormOrderOther($request->all())->validate();

		return $request->all();
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validateFormService(array $data)
	{
		return Validator::make($data, [
			'type_service'  => ['required'],
			'article_id'    => ['required'],
			'description'   => ['required'],
			'quantity'      => ['required'],
			'unitary_price' => ['required'],
			'load_type'     => ['required'],
			'height'        => ['nullable'],
			'high'          => ['nullable'],
			'width'         => ['nullable'],
			'weight'        => ['required'],
			're_weight'     => ['nullable'],
			'content'       => ['nullable']
		]);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validateFormProduct(array $data)
	{
		return Validator::make($data, [
			'type_service'  => ['required'],
			'article_id'    => ['required'],
			'description'   => ['required'],
			'quantity'      => ['required'],
			'unitary_price' => ['required'],
			'height'        => ['nullable'],
			'high'          => ['nullable'],
			'width'         => ['nullable'],
			'content'       => ['nullable']
		]);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validateCrateDimensionsResidentials(array $data)
	{
		return Validator::make($data, [
			'height' => ['required', 'numeric', 'min:5'],
			'high'   => ['required', 'numeric', 'min:5'],
			'width'  => ['required', 'numeric'],
		]);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validateCrateDimensionsNotResidentials(array $data)
	{
		return Validator::make($data, [
			'height' => ['required', 'numeric', 'min:12'],
			'high'   => ['required', 'numeric', 'min:12'],
			'width'  => ['required', 'numeric', 'min:12'],
		]);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validateFormPrice(array $data)
	{
		return Validator::make($data, [
			'after'  => ['required', 'numeric'],
			'reason' => ['required', 'max:100']
		]);
	}

	public function validatePrice(Request $request)
	{
		$this->validateFormPrice($request->all())->validate();

		$data = [
			'message' => trans('messages.success'),
			'price' => $request->all()
		];

		return response()->json($data, 200);
	}

	public function validateDetail(Request $request)
	{
		if (($request['type'] === 2)) $this->validateFormService($request->all())->validate();
		else $this->validateFormProduct($request->all())->validate();

		if (($request['crate']) && ($request['load_type'] === 'residencial')) $this->validateCrateDimensionsResidentials($request->all())->validate();
		elseif (($request['crate']) && ($request['load_type'] !== 'residencial')) $this->validateCrateDimensionsNotResidentials($request->all())->validate();

		$data = [
			'message' => trans('messages.success'),
			'detail' => $request->all()
		];

		return response()->json($data, 200);
	}
}
