<?php

namespace App\Http\Controllers;

use App\File;
use App\Http\Traits\ParseFormat;
use App\Http\Traits\PDFs;
use App\Order;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class DocumentsController extends Controller
{
    use PDFs, ParseFormat;

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function fileQuote($id)
    {
        $order = Order::with([
            'user',
            'file',
            'activity',
            'account',
            'details',
            'customer',
            'addre.customer',
            'details.product',
            'details.service',
        ])->findOrFail($id);

        if ($order->activity->contains('pivot.activity_id', 1)) {

            $activity_delivery = $order->activity->where('pivot.activity_id', 1);
            $count = $activity_delivery->count();

            if ($count > 1) {

                $delivery = $activity_delivery->get();
                $order->activity_delivery = $this->parseFormat($delivery[0]->pivot->work_date) . ' - ' . $this->parseFormat($delivery[$count - 1]->pivot->work_date);
            } else {

                $order->activity_delivery = $this->parseFormat($activity_delivery->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 2)) {

            $activity_packaging_date = $order->activity->where('pivot.activity_id', 2);
            $count = $activity_packaging_date->count();

            if ($count > 1) {

                $packaging_date = $activity_packaging_date->get();
                $order->activity_packaging_date = $this->parseFormat($packaging_date[0]->pivot->work_date) . ' - ' . $this->parseFormat($packaging_date[$count - 1]->pivot->work_date);
            } else {

                $order->activity_packaging_date = $this->parseFormat($activity_packaging_date->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 3)) {

            $activity_unpack_range = $order->activity->where('pivot.activity_id', 3);
            $count = $activity_unpack_range->count();

            if ($count > 1) {

                $unpack_range = $activity_unpack_range->get();
                $order->activity_unpack_range = $this->parseFormat($unpack_range[0]->pivot->work_date) . ' - ' . $this->parseFormat($unpack_range[$count - 1]->pivot->work_date);
            } else {

                $order->activity_unpack_range = $this->parseFormat($activity_unpack_range->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 4)) {

            $activity_load_range = $order->activity->where('pivot.activity_id', 4);
            $count = $activity_load_range->count();

            if ($count > 1) {

                $load_range = $activity_load_range->get();
                $order->activity_load_range = $this->parseFormat($load_range[0]->pivot->work_date) . ' - ' . $this->parseFormat($load_range[$count - 1]->pivot->work_date);
            } else {

                $order->activity_load_range = $this->parseFormat($activity_load_range->first()->pivot->work_date);
            }
        }

        if (count($order->activity) > 0) {
            $order->services = $order->activity->unique('pivot.activity_id')->sortBy('pivot.id')->implode('name', ', ');
        }

        $path = $this->createFileQuotation($order);

        if ($find = $order->file()->where('type', 'quote')->first()) {
            $file = File::findOrFail($find['id']);
        } else {
            $file = new File();
        }

        $file->order_id = $order['id'];
        $file->url_document = $path;
        $file->type = 'quote';
        $file->save();

        return $file;
    }

    public function updateFile($id)
    {
        $order = Order::with([
            'user',
            'file',
            'activity',
            'account',
            'details',
            'customer',
            'addre.customer',
            'details.product',
            'details.service',
        ])->findOrFail($id);

        $order->assignments = $order->assignments->unique('user_id');

        if ($order->activity->contains('pivot.activity_id', 1)) {

            $activity_delivery = $order->activity->where('pivot.activity_id', 1);
            $count = $activity_delivery->count();

            if ($count > 1) {

                $delivery = $activity_delivery->get();
                $order->activity_delivery = $this->parseFormat($delivery[0]->pivot->work_date) . ' - ' . $this->parseFormat($delivery[$count - 1]->pivot->work_date);
            } else {

                $order->activity_delivery = $this->parseFormat($activity_delivery->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 2)) {

            $activity_packaging_date = $order->activity->where('pivot.activity_id', 2);
            $count = $activity_packaging_date->count();

            if ($count > 1) {

                $packaging_date = $activity_packaging_date->get();
                $order->activity_packaging_date = $this->parseFormat($packaging_date[0]->pivot->work_date) . ' - ' . $this->parseFormat($packaging_date[$count - 1]->pivot->work_date);
            } else {

                $order->activity_packaging_date = $this->parseFormat($activity_packaging_date->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 3)) {

            $activity_unpack_range = $order->activity->where('pivot.activity_id', 3);
            $count = $activity_unpack_range->count();

            if ($count > 1) {

                $unpack_range = $activity_unpack_range->get();
                $order->activity_unpack_range = $this->parseFormat($unpack_range[0]->pivot->work_date) . ' - ' . $this->parseFormat($unpack_range[$count - 1]->pivot->work_date);
            } else {

                $order->activity_unpack_range = $this->parseFormat($activity_unpack_range->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 4)) {

            $activity_load_range = $order->activity->where('pivot.activity_id', 4);
            $count = $activity_load_range->count();

            if ($count > 1) {

                $load_range = $activity_load_range->get();
                $order->activity_load_range = $this->parseFormat($load_range[0]->pivot->work_date) . ' - ' . $this->parseFormat($load_range[$count - 1]->pivot->work_date);
            } else {

                $order->activity_load_range = $this->parseFormat($activity_load_range->first()->pivot->work_date);
            }
        }

        if (count($order->activity) > 0) {
            $order->services = $order->activity->unique('pivot.activity_id')->sortBy('pivot.id')->implode('name', ', ');
        }

        $orderType = $order->type === 'C' ? 'quote' : 'workorder';

        $file = $order->file()->where('type', $orderType)->first();

        if (!empty($file)) {
            if ($source = explode('storage/', $file->url_document)) {
                if (Storage::disk('public')->exists($source[1])) {
                    Storage::disk('public')->delete($source[1]);
                }
            }
        }

        if (empty($file)) $file = new File();
        $file->order_id = $order['id'];
        $file->url_document = $order->type === 'C' ? $this->createFileQuotation($order) : $this->createFileWorkOrder($order);
        $file->type = $order->type === 'C' ? 'quote' : 'workorder';
        $file->save();

        return $file;
    }
}
