<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use Mail;
use App\User;
use Carbon\Carbon;
use App\Mail\EmailConfirmation;

class UsersController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $me = $request->user();

        $users = User::with([
            'position', 
            'account'
        ])->where([ 
            ['account_id', $me['id']] 
        ])->orderBy('id', 'DESC')->get()->except(1);

        return $users;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormStore(array $data)
    {
        return Validator::make($data, [
            'name'              => ['required'],
            'email'             => ['required', 'email', 'unique:users'],
            'address'           => ['required'],
            'phone'             => ['required', 'numeric', 'digits_between:7,10'],
            'country_alphacode' => ['required'],
            'mobile'            => ['nullable', 'digits_between:7,10'],
            'postal_code'       => ['required', 'numeric'],
            'user'              => ['required', 'alpha_dash'],
            'password'          => ['required', 'string', 'min:6'],
            'state'             => ['required'],
            'city'              => ['required']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->ValidatorFormStore($request->all())->validate();

        $me = $request->user();

        $user = new User();
        $user->user_id = $me['id'];
        $user->property = 'E';
        $user->account_id = $me['account_id'];
        $user->password = bcrypt($request['password']);
        $user->encrypted_confirmation = str_random(25);
        $user->status = true;
        $user->fill($request->except(['user_id', 'property', 'account_id', 'password', 'encrypted_confirmation', 'status']))->save();

        Mail::to($user->email)->send(new EmailConfirmation($user));

        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return $user;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormUpdate(array $data, $id)
    {
        return Validator::make($data, [
            'name'              => ['required'],
            'email'             => ['required', Rule::unique('users', 'email')->ignore($id)],
            'address'           => ['required'],
            'phone'             => ['required', 'numeric', 'digits_between:7,10'],
            'country_alphacode' => ['required'],
            'mobile'            => ['nullable', 'digits_between:7,10'],
            'postal_code'       => ['required', 'numeric'],
            'user'              => ['required', 'alpha_dash'],
            'password'          => ['nullable', 'string', 'min:6'],
            'state'             => ['required'],
            'city'              => ['required']
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->ValidatorFormUpdate($request->all(), $id)->validate();

        $me = $request->user();

        $user = User::findOrFail($id);
        $user->user_id = $me['id'];
        $user->password = !empty($request['password']) ? bcrypt($request['password']) : $user->password;
        $user->fill($request->except(['password', 'user_id']))->save();

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get the specified resource filtered position.
     *
     * @param  int  $position_id
     * @return \Illuminate\Http\Response
     */
    public function getByPosition($position_id)
    {
        return User::where('position_id', $position_id)->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changeStatusUser($id)
    {
        $user = User::with(['position', 'account'])->findOrFail($id);
        $user->status = ( ($user->status) ) ? false : true;
        $user->inactive_from = ( ($user->status) ) ? null : Carbon::now();
        $user->save();
        
        return $user;
    }
}
