<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\File;
use App\Order;
use App\Detail;
use App\Account;
use App\Service;
use App\Product;
use App\General;
use App\Activity;
use App\TermsDimension as Terms;

use Mail;

use App\Http\Traits\PDFs;
use App\Mail\EmailQuote;

define('CANCELED', 0);
define('PENDING', 1);
define('AUTHORIZED', 2);
define('PROCESS', 3);

class LandingController extends Controller
{
  use PDFs;

  public function getAccount(Request $request)
  {
    $account = Account::find(1);

    return $account;
  }

  public function getActivities()
  {
    $activities = Activity::all();

    return $activities;
  }

  public function getTypeServices()
  {
    $types = Service::get()->groupBy('type');

    return $types;
  }

  public function getTypeProducts()
  {
    $types = Product::get()->groupBy('type');

    return $types;
  }

  public function getOnlyService($Id)
  {
    $service = Service::findOrFail($Id);

    return $service;
  }

  public function getOnlyProduct($Id)
  {
    $product = Product::findOrFail($Id);

    return $product;
  }

  public function getGenerals(Request $request)
  {
    $general = General::findOrFail(1);

    return $general;
  }

  public function getOnlyTerm(Request $request)
  {
    $terms  = Terms::all();

    $width = $request->Width;

    foreach ($terms as $key => $term) {
      if (($width >= $term->min) && ($width <= $term->max)) return ['value' => $term->value, 'type' => 'REP'];
      elseif ((is_null($term->max)) && ($width >= $term->min)) return ['value' => $term->value, 'type' => 'INC'];
    }
  }

  public function getId()
  {
    $identifier = strtoupper(str_random(10));

    return $identifier;
  }

  public function storeQuotations(Request $request)
  {
    $general = General::findOrFail(1);

    $percentage_card = $general->card_payment / 100;

    $order = new Order();
    $order->identifier = $this->getId();
    $order->account_id = 1;
    $order->user_id = 1;
    $order->type = 'C';
    $order->no_art = 0;
    $order->com_payment = ($request['type_payment'] === "2" || $request['type_payment'] === "3") ? $request['total'] * $percentage_card : 0;
    $order->card_payment = ($request['type_payment'] === "2" || $request['type_payment'] === "3") ? $percentage_card : 0;
    $order->total = $request['total'] + $order->com_payment;

    $order->fill($request->except([
      'identifier',
      'account_id',
      'type',
      'no_art',
      'com_payment',
      'card_payment',
      'total',
      'user_id'
    ]))->save();

    return $order;
  }

  public function storeDetails($Id, Request $request)
  {
    $order = Order::findOrFail($Id);

    foreach ($request->all() as $key => $article) {
      $detail = new Detail();
      $detail->order_id = $Id;
      $detail->weight_vol = 0;
      $detail->fill(collect($article)->except(['order_id', 'weight_vol'])->toArray())->save();
    }

    $order->no_art = count($request->all());
    $order->save();

    return $order;
  }

  public function storeFile(Request $request)
  {
    $order = Order::with([
      'user',
      'file',
      'account',
      'details',
      'customer',
      'addre.customer',
      'details.product',
      'details.service'
    ])->findOrFail($request->id);

    $path = $this->createFileQuotation($order);

    if ($find = $order->file()->where('type', 'quote')->first()) $file = File::findOrFail($find['id']);
    else $file = new File();

    $file->order_id = $request->id;
    $file->url_document = $path;
    $file->type = 'quote';
    $file->save();

    $order->file = $file;

    Mail::to('operations@cmlexports.com')->send(new EmailQuote($order));

    return $file;
  }
}
