<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;

class LanguageController extends Controller
{
    /**
 * @param $lang
 *
 * @return \Illuminate\Http\RedirectResponse
 */
    public function swap($lang)
    {
      // Almacenar el lenguaje en la session
      session()->put('locale', $lang);
      return $lang;
    }
}