<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Customer;

class ValidLandingController extends Controller
{
  protected function ValidQuote(array $data)
  {
    return Validator::make($data, [
      'invoice_to'             => ['nullable'],
      'type_location'          => ['nullable'],
      'instructions_especials' => ['nullable'],
      'price'                  => ['nullable'],
      'work_date'              => ['nullable'],
      'estimated_value'        => ['nullable'],
      'work_begins'            => ['nullable'],
      'work_ends'              => ['nullable'],
      'cub_foot'               => ['nullable'],
      'weight'                 => ['nullable'],
      'delivery_date'          => ['nullable'],
      'miles'                  => ['nullable'],
      'collect_date'           => ['nullable'],
      'type_payment'           => ['required'],
      'warehouse'              => ['nullable'],
      'dist_weight'            => ['nullable']
    ]);
  }

  public function formQuote(Request $request)
  {
    $this->ValidQuote($request->all())->validate();

    return $request->all();
  }

  protected function ValidCustomer(array $data)
  {
    return Validator::make($data, [
      'name'              => ['required'],
      'address'           => ['nullable'],
      'country_alphacode' => ['required'],
      'postal_code'       => ['required', 'numeric'],
      'state'             => ['required'],
      'city'              => ['required'],
      'phone'             => ['required', 'numeric', 'digits_between:7,10'],
      'email'             => ['required', 'email']
    ]);
  }

  public function formCustomer(Request $request)
  {
    $this->ValidCustomer($request->all())->validate();

    $haveAccess = false;

    $customer = new Customer();
    $customer->user_id = 1;
    $customer->account_id = 1;
    $customer->haveAccess = $haveAccess;
    $customer->fill($request->except(['user_id', 'account_id', 'haveAccess']))->save();

    return $customer;
  }

  protected function ValidService(array $data)
  {
    return Validator::make($data, [
      'type_service'  => ['required'],
      'article_id'    => ['required'],
      'description'   => ['required'],
      'quantity'      => ['required'],
      'unitary_price' => ['required'],
      'load_type'     => ['required'],
      'height'        => ['nullable'],
      'high'          => ['nullable'],
      'width'         => ['nullable'],
      'weight'        => ['required'],
      're_weight'     => ['nullable'],
      'content'       => ['nullable']
    ]);
  }

  protected function ValidProduct(array $data)
  {
    return Validator::make($data, [
      'type_service'  => ['required'],
      'article_id'    => ['required'],
      'description'   => ['required'],
      'quantity'      => ['required'],
      'unitary_price' => ['required'],
      'height'        => ['nullable'],
      'high'          => ['nullable'],
      'width'         => ['nullable'],
      'content'       => ['nullable']
    ]);
  }

  protected function validateCrateDimensionsResidentials(array $data)
  {
    return Validator::make($data, [
      'height' => ['required', 'numeric', 'min:5'],
      'high'   => ['required', 'numeric', 'min:5'],
      'width'  => ['required', 'numeric']
    ]);
  }

  protected function validateCrateDimensionsNotResidentials(array $data)
  {
    return Validator::make($data, [
      'height' => ['required', 'numeric', 'min:12'],
      'high'   => ['required', 'numeric', 'min:12'],
      'width'  => ['required', 'numeric', 'min:12'],
    ]);
  }

  public function formDetail(Request $request)
  {
    if (($request['type'] === 2)) $this->ValidService($request->all())->validate();
    else $this->ValidProduct($request->all())->validate();

    if (($request['crate']) && ($request['load_type'] === 'residencial')) $this->validateCrateDimensionsResidentials($request->all())->validate();
    elseif (($request['crate']) && ($request['load_type'] !== 'residencial')) $this->validateCrateDimensionsNotResidentials($request->all())->validate();

    $data = [
      'message' => trans('messages.success'),
      'detail' => $request->all()
    ];

    return response()->json($data, 200);
  }
}
