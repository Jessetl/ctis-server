<?php

namespace App\Http\Controllers\Generals;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Pound;
use App\Conditional;

class PoundsController extends Controller
{
    public function updatePoundWithConditionals(Request $request, $id)
    {
    	$me = $request->user();

    	$pound = Pound::findOrFail($id);
    	$pound->user_id = $me['id'];
    	$pound->fill($request->except('user_id'))->save();

    	$conditionals = $request->conditionals;

    	foreach ($conditionals as $key => $conditional) {

    		$condition = Conditional::findOrFail($conditional['id']);
    		$condition->user_id = $me['id'];
    		$condition->fill(collect($conditional)->except(['user_id', 'id', 'pound_id'])->toArray())->save();
    	}

    	return $pound;
    }
}
