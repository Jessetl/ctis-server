<?php

namespace App\Http\Controllers\Generals;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Position;

class PositionsController extends Controller
{
    public function getPositionsWithConditionals()
    {
    	$positions = Position::with([
    		'pounds',
    		'pounds.conditionals'
    	])->get()->except([1, 2, 3, 4]);

    	return $positions;
    }
}
