<?php

namespace App\Http\Controllers\Payrolls;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Concept;

class ConceptsController extends Controller
{
	public function getConcepts(Request $request)
	{
		$concepts = Concept::where([
			
			['visibility', 1]

		])->get();

		return $concepts;
	}
}