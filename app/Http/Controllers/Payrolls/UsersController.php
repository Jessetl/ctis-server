<?php

namespace App\Http\Controllers\Payrolls;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use App\Salary;

class UsersController extends Controller
{
	/**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAssistsByUser(Request $request)
    {
        $me = $request->user();
        $typePayroll = $request['payroll'] ? 'S' : 'Q';

        $from = $request['begins'];
        $to = $request['finish'];

        $users = User::with([

            'salary',
            'position',
            'assignments',
            'assignments.order' => function ($query) use ($from, $to) {
                $query->whereBetween('work_date', [$from, $to]);
            },
            'position.pounds',
            'position.pounds.conditionals',
            'assists.hours',
            'assists' => function ($query) use ($from, $to) {
                $query->whereBetween('date', [$from, $to]);
            }

        ])->whereHas('salary', function ($query) use ($typePayroll) {
            
            $query->where('payroll_type', $typePayroll);

        })->where([

            ['property', 'E'], 
            ['account_id', $me['account_id']], 
            ['salaryActive', true], 
            ['status', true] 
            
        ])->orderBy('name', 'ASC')->get();

        return $users;
    }
}