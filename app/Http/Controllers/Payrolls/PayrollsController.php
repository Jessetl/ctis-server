<?php

namespace App\Http\Controllers\Payrolls;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\User;
use App\Payroll;
use Carbon\Carbon;

class PayrollsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormGenerate(array $data)
    {
        return Validator::make($data, [
            'payroll' => ['required', 'boolean'],
            'begins'  => ['required'],
            'finish'  => ['required']
        ]);
    }

    /**
     * Validate the form to create a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function payrollValidation(Request $request)
    {
        $this->ValidatorFormGenerate($request->all())->validate();

        $exists = Payroll::where([ 
            ['payroll', $request['payroll']], 
            ['begins', $request['begins']], 
            ['finish', $request['finish']] 
        ]);

        if ( ($exists->exists()) ) {
            return response()->json(['payroll' => $exists->first(), 'message' => 'Already stored information'], 422); }

        return $request->all();
        /*$me = $request->user();

        $type_payroll = $request['payroll'] ? 1 : 2;
        $identifier = strtoupper(Carbon::now()->format('Y') . Carbon::now()->format('m') . $type_payroll . str_random(5));

        $payroll = new Payroll();
        $payroll->identifier = $identifier;
        $payroll->user_id = $me['id'];
        $payroll->fill($request->except(['identifier', 'user_id']))->save();
        
        return $payroll;*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
