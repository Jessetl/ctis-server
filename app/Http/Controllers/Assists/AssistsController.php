<?php

namespace App\Http\Controllers\Assists;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Hour;
use DateTime;
use App\Order;
use App\Assistance;

class AssistsController extends Controller
{
	/**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $assists = Assistance::with([
            'user', 
            'order',
            'hours'
        ])->orderBy('id', 'DESC')->get();

        return $assists;
    }

    public function assists(Request $request, $id)
    {
    	$order = Order::with([
             
            'assignments',
            'assignments.user',
            'assignments.user.salary',
            'assignments.user.position',
            'assignments.user.position.pounds',
            'assignments.user.position.pounds.conditionals'

        ])->findOrFail($id);
    	
        $assignments = $order->assignments;

    	foreach ($assignments as $key => $assignment) {

        	$work_begins = new DateTime($order['work_begins']);
        	$work_ends = new DateTime($order['work_ends']);
        	$hours = $work_begins->diff($work_ends);

            $hours = new Hour([
                'work_begins' => $work_begins, 
                'work_ends' => $work_ends, 
                'hours' => $hours->format('%H:%I:%S')
            ]);

            $assistance = new Assistance();
            $assistance->order_id = $order['id'];
            $assistance->user_id = $assignment['user_id'];
            $assistance->date = $order['work_date'];
            $assistance->haveTravelTime = 0;
            $assistance->save();

            $assistance->hours()->save($hours);
        }

        return $order;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormStore(array $data)
    {
        return Validator::make($data, [
            'user_id'     => ['required'],
            'work_begins' => ['required'],
            'work_ends'   => ['required'],
            'date'        => ['required'],
            'hours'       => ['required']
        ]);
    }

    public function store(Request $request)
    {
        $this->ValidatorFormStore($request->all())->validate();

        $exists = Assistance::where([
            ['user_id', $request['user_id']], 
            ['date', $request['date']] 
        ])->exists();
        
        if ( is_null($request->order_id) ) {
            if ( ($exists) ) return response()->json(['message' => 'User already exists on this date'], 422);
        }

        $me = $request->user();

        $hour = new Hour();
        $hour->fill($request->all());

        $assistance = new Assistance();
        $assistance->fill($request->all())->save();

        $assistance->hours()->save($hour);

        return $assistance;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $assistance = Assistance::with([
            'user',
            'order',
            'hours'
        ])->findOrFail($id);

        return $assistance;
    }

    public function assistsHours(Request $request)
    {
        $assistance = Assistance::findOrFail($request['id']);

        $hour = new Hour();
        $hour->fill($request->all());

        $assistance->hours()->save($hour);

        return $assistance;
    }
}
