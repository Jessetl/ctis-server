<?php

namespace App\Http\Controllers\Assists;

use App\Assignment;
use App\Http\Controllers\Controller;

class AssignmentsController extends Controller
{
    public function getUserAssignments($id)
    {
        $assignments = Assignment::with([

            'user',
            'order',
            'activity',

        ])->where([

            ['user_id', $id],
            ['status', 0],

        ])->get();

        return $assignments;
    }
}
