<?php

namespace App\Http\Controllers\Assists;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Order;

class OrdersController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getOrders(Request $request)
    {
        $me = $request->user();

        $orders = Order::where([
            ['type', 'O'],
            ['status', 1]
        ])->get();

        return $orders;
    }
}