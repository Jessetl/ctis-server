<?php

namespace App\Http\Controllers\Assists;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;

class UsersController extends Controller
{
	/**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $me = $request->user();

        $users = User::where([ 
            ['property', 'E'], 
            ['account_id', $me['id']], 
            ['salaryActive', true], 
            ['status', true] 
        ])->orderBy('name', 'ASC')->get();

        return $users;
    }
}