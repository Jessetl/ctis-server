<?php

namespace App\Http\Controllers\Assists;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Position;

class PositionsController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPositionsWithUsers(Request $request)
    {
        $me = $request->user();

        $positions = Position::with([
            'users' => function ($query) use ($me) {
               $query->where([ 
                    ['property', 'E'], 
                    ['account_id', $me['id']], 
                    ['salaryActive', true], 
                    ['status', true] 
               ]);
            }
        ])->get()->except(1);

        return $positions;
    }
}