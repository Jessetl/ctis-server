<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use Mail;
use App\User;
use App\Addre;
use App\Customer;
use App\Mail\EmailConfirmation;

class CustomersController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::with('account')->orderBy('id', 'DESC')->get();

        return $customers;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormStore(array $data)
    {
        return Validator::make($data, [
            'name'              => ['required'],
            'address'           => ['required'],
            'email'             => ['required', 'email', 'unique:users'],
            'country_alphacode' => ['required'],
            'postal_code'       => ['required', 'numeric'],
            'state'             => ['required'],
            'city'              => ['required'],
            'phone'             => ['required', 'numeric', 'digits_between:7,10'],
            'user'              => ['nullable', 'alpha_dash', 'unique:users'],
            'password'          => ['nullable', 'string', 'min:6']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->ValidatorFormStore($request->all())->validate();

        $me = $request->user();
        $haveAccess = false;

        if ( !empty($request['user']) OR !empty($request['password']) ) {

            $request->password = bcrypt($request['password']);

            $user = new User();
            $user->user_id = $me['id'];
            $user->account_id = $me['account_id'];
            $user->property = 'C';
            $user->password = bcrypt($request['password']);
            $user->encrypted_confirmation = str_random(25);
            $user->status = true;
            $user->fill($request->except(['user_id', 'account_id', 'property', 'password', 'status', 'encrypted_confirmation']))->save();

            Mail::to($user->email)->send(new EmailConfirmation($user));

            $haveAccess = true;
        }

        $customer = new Customer();
        $customer->user_id = $me['id'];
        $customer->account_id = $me['account_id'];
        $customer->haveAccess = $haveAccess;
        $customer->fill($request->except(['user_id', 'account_id', 'haveAccess']))->save();

        return response()->json([
            'customer' => $customer,
            'message' => trans('messages.success')
        ]);
    }

     /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormStoreFromOrder(array $data)
    {
        return Validator::make($data, [
            'name'              => ['required'],
            'address'           => ['required'],
            'email'             => ['nullable', 'string', 'email', 'unique:users'],
            'country_alphacode' => ['required'],
            'postal_code'       => ['required', 'numeric'],
            'state'             => ['required'],
            'city'              => ['required'],
            'phone'             => ['required', 'numeric', 'digits_between:7,10']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCustomerFromOrder(Request $request, $id)
    {
        $this->ValidatorFormStoreFromOrder($request->all())->validate();

        $me = $request->user();
        $haveAccess = false;

        $customer = new Customer();
        $customer->user_id = $me['id'];
        $customer->account_id = $id;
        $customer->haveAccess = $haveAccess;
        $customer->status = true;
        $customer->fill($request->except(['user_id', 'account_id', 'status', 'haveAccess']))->save();
        
        return $customer;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->user = $customer->haveAccess ? User::where('email', $customer->email)->first() : null;
        
        return $customer;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormUpdate(array $data, $id)
    {
        return Validator::make($data, [
            'name'              => ['required'],
            'address'           => ['required'],
            'email'             => ['required', 'email', Rule::unique('customers', 'email')->ignore($id)],
            'country_alphacode' => ['required'],
            'postal_code'       => ['required', 'numeric'],
            'state'             => ['required'],
            'city'              => ['required'],
            'phone'             => ['required', 'numeric', 'digits_between:7,10']
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorFormUserUpdate(array $data, $id)
    {
        return Validator::make($data, [
            'user'     => ['required', 'alpha_dash', Rule::unique('users', 'user')->ignore($id)],
            'password' => ['nullable', 'string', 'min:6']
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->ValidatorFormUpdate($request->all(), $id)->validate();
        
        $me = $request->user();
        $customer = Customer::findOrFail($id);
        $userFromCustomer = $customer->haveAccess ? User::where('email', $customer->email)->first() : null;

        $haveAccess = $customer->haveAccess;
        
        if ( (!empty($request['user']) OR !empty($request['password'])) AND (!$customer['haveAccess']) ) {

            $user = new User();
            $user->user_id = $me['id'];
            $user->account_id = $me['account_id'];
            $user->property = 'C';
            $user->password = bcrypt($request['password']);
            $user->encrypted_confirmation = str_random(25);
            $user->status = true;
            $user->fill($request->except(['user_id', 'account_id', 'property', 'password', 'status', 'encrypted_confirmation']))->save();

            Mail::to($user->email)->send(new EmailConfirmation($user));
            
            $haveAccess = true;

        } elseif ( (!empty($request['password'])) AND ($customer['haveAccess']) ) {

            $this->validatorFormUserUpdate($request->all(), $userFromCustomer['id'])->validate();

            $user = $userFromCustomer;
            $user->user = $request['user'];
            $user->password = bcrypt($request['password']);
            $user->save();

        } elseif ( (!empty($request['user'])) AND ($customer['haveAccess']) ) {

            $this->validatorFormUserUpdate($request->all(), $userFromCustomer['id'])->validate();
            
            $user = $userFromCustomer;
            $user->user = $request['user'];
            $user->save();
        }

        $customer->user_id = $me['id'];
        $customer->haveAccess = $haveAccess;
        $customer->fill($request->except(['user_id', 'haveAccess']))->save();
        
        return $customer;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormUpdateFromOrder(array $data)
    {
        return Validator::make($data, [
            'name'              => ['required'],
            'address'           => ['required'],
            'email'             => ['nullable', 'email', 'unique:users'],
            'country_alphacode' => ['required'],
            'postal_code'       => ['required', 'numeric'],
            'state'             => ['required'],
            'city'              => ['required'],
            'phone'             => ['required', 'numeric', 'digits_between:7,10']
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateAddressFromOrder(Request $request, $id)
    {
        $this->ValidatorFormUpdateFromOrder($request->all())->validate();

        $me = $request->user();
        
        $customer = Customer::findOrFail($id);
        $addre = Addre::findOrFail($request['addre_id']);
        $addre->fill($request->all())->save();

        $customer->load('address');

        return response()->json([
            'customer' => $customer,
            'message' => trans('messages.success')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCustomerFromOrder(Request $request, $id)
    {
        $this->ValidatorFormUpdateFromOrder($request->all())->validate();

        $me = $request->user();
        
        $customer = Customer::findOrFail($id);
        $customer->fill($request->all())->save();

        return response()->json([
            'customer' => $customer,
            'message' => trans('messages.success')
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormAddress(array $data)
    {
        return Validator::make($data, [
            'address'           => ['required'],
            'country_alphacode' => ['required'],
            'postal_code'       => ['required', 'numeric'],
            'state'             => ['required'],
            'city'              => ['required']
        ]);
    }

    public function address(Request $request, $id)
    {
        $this->ValidatorFormAddress($request->all())->validate();

        $customer = Customer::findOrFail($id);

        $addre = new Addre();
        $addre->customer_id = $customer['id'];
        $addre->fill($request->except('customer_id'))->save();

        $customer->load('address');

        return response()->json([
            'customer' => $customer,
            'addre' => $addre,
            'message' => trans('messages.success')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     /**
     * Get costumers by name.
     *
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    public function getByName ($account, $name)
    {
        return Customer::where([ ['account_id', $account], ['name', 'like', '%'. $name .'%'] ])->get();
    }

     /**
     * Get address of customer.
     *
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */

    public function getAddre($id)
    {
        return Addre::findOrFail($id);
    }

     /**
     * Get address of customer.
     *
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */

    public function getAddress($id)
    {
        return Addre::where('customer_id', $id)->get();
    }

    public function changeStatusCustomer($id)
    {
        $customer = Customer::with('account')->findOrFail($id);
        $customer->status = ( ($customer->status) ) ? false : true;
        $customer->save();
        
        return $customer;
    }
}
