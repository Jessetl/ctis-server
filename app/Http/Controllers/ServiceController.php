<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ServiceRequest;
use Illuminate\Support\Facades\Validator;

use App\Service;
use App\Product;
use App\User;

class ServiceController extends Controller
{
     /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $me = $request->user();

        if ($me->position_id == 1) {
            $services = Service::latest()->get();
        }

        return $services;
    }

     /**
     * Return identifier.
     *
     * @return \Illuminate\Http\Response
     */
    public function getId()
    {
        $identifier = $this->generateId();

        return ['identifier' => $identifier];
    }

    /**
     * Generate a identifier unique.
     *
     * @return int identifier
     */
    private function generateId()
    {
        $identifier = strtoupper(str_random(5));

        $exists_service = Service::where('identifier', $identifier)->exists();
        $exists_product = Product::where('identifier', $identifier)->exists();

        if ($exists_product || $exists_service) {
            $this->generateId();
        }

        return $identifier;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormStore(array $data)
    {
        return Validator::make($data, [
            'identifier'  => ['required', 'unique:services'],
            'type'        => ['required'],
            'name'        => ['required'],
            'description' => ['required'],
            'account_id'  => ['required'],
            'account_id'  => ['required'],
            'price'       => ['required'],
            'cost'        => ['required', 'numeric'],
            'load'        => ['nullable', 'numeric'],
            'max-weight'  => ['nullable', 'numeric']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->ValidatorFormStore($request->all())->validate();
        
        $me = $request->user();
        $stringCrate = 'CRATE';
        $stringUncrate = 'UNCRATE';

        $service = new Service();
        $service->crate = (stristr($request->name, $stringCrate) || stristr($request->description, $stringCrate)) ? true : false;
        $service->uncrate = (stristr($request->name, $stringUncrate) || stristr($request->description, $stringUncrate)) ? true : false;
        $service->fill($request->except(['crate', 'uncrate']))->save();

        return $service;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::findOrFail($id);
        
        return $service;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormUpdate(array $data)
    {
        return Validator::make($data, [
            'type'        => ['required'],
            'name'        => ['required'],
            'description' => ['required'],
            'account_id'  => ['required'],
            'price'       => ['required'],
            'cost'        => ['required', 'numeric'],
            'load'        => ['nullable', 'numeric'],
            'max-weight'  => ['nullable', 'numeric']
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->ValidatorFormUpdate($request->all())->validate();

        $me = $request->user();
        $stringCrate = 'CRATE';
        $stringUncrate = 'UNCRATE';
        
        $service = Service::findOrFail($id);
        $service->crate = (stristr($request->name, $stringCrate) || stristr($request->description, $stringCrate)) ? true : false;
        $service->uncrate = (stristr($request->name, $stringUncrate) || stristr($request->description, $stringUncrate)) ? true : false;
        $service->fill($request->except(['crate', 'uncrate', 'identifier']))->save();

        return $service;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        //
    }

    /**
     * Get services filtered for type service
     *
     * @param  int $type_id
     * @return \Illuminate\Http\Response
     */
    public function getByType($account, $type)
    {
        $services = Service::where([ ['account_id', $account], ['type', $type] ])->groupBy('name')->pluck('name')->toArray();

        return $services;
    }

    /**
     * Get services filtered for type service
     *
     * @param  int $type_id
     * @return \Illuminate\Http\Response
     */
    public function getByName($account, $name)
    {
        $services = Service::where([ ['account_id', $account], ['name', $name] ])->get();

        return $services;
    }
}
