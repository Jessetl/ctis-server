<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Account;
use App\Expense;
use App\Provider;

use Storage;
use Carbon\Carbon;

class ExpensesController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = Carbon::now();

        $expenses = Expense::with(['service', 'provider'])->whereMonth('date', $today->month)->get();

        return $expenses;
    }

    /**
     * Return identifier.
     *
     * @return \Illuminate\Http\Response
     */
    public function getId()
    {
        $identifier = strtoupper(str_random(10));

        return $identifier;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormStore(array $data)
    {
        return Validator::make($data, [
            'type_service_id' => ['required'],
            'provider_id'     => ['required'],
            'date'            => ['required'],
            'description'     => ['required'],
            'amount'          => ['required', 'between:0,99.99'],
            'tax'             => ['required', 'between:0,99.99'],
            'expense'         => ['required', 'between:0,99.99'],
            'folio'           => ['required'],
            'notes'           => ['nullable', 'max:250'],
            'voucher'         => ['nullable']
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormFile(array $data)
    {
        return Validator::make($data, [
            'file' => ['max:4000', 'mimes:jpeg,png,pdf']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->ValidatorFormStore($request->all())->validate();
        $this->ValidatorFormFile($request->all())->validate();

        $me = $request->user();
        $account = Account::findOrFail($me->account_id);

        $expense = new Expense();
        $expense->user_id = $me['id'];
        $expense->voucher = NULL;
        $expense->fill($request->except(['voucher', 'user_id']))->save();

        if ( ($request->file('voucher')) ) {

            $ext = $request->file('voucher')->getClientOriginalExtension();
            $fileOriginalName =  pathinfo($request->file('voucher')->getClientOriginalName(), PATHINFO_FILENAME);
          
            $folder = 'public/' . $account->name . '/' . 'EXPENSES' . '/' . Carbon::now()->format('Y') . '/' . Carbon::now()->format('m');
            $folderNotPublic = $account->name . '/' . 'EXPENSES' . '/' . Carbon::now()->format('Y') . '/' . Carbon::now()->format('m');
            $fileName = Carbon::now()->format('Y') . Carbon::now()->format('m') . Carbon::now()->format('d') . '_' . $this->getId() . '.' . $ext;

            if (! Storage::exists($folder)) Storage::makeDirectory($folder);

            Storage::putFileAs($folder, $request->voucher, $fileName);

            $expense->voucher = env('APP_URL') . 'storage/' . $folderNotPublic . '/' . $fileName;
            $expense->save();   
        } 

        return $expense;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expense = Expense::with('provider')->findOrFail($id);

        return $expense;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormUpdate(array $data)
    {
        return Validator::make($data, [
            'type_service_id' => ['required'],
            'provider_id'     => ['required'],
            'date'            => ['required'],
            'description'     => ['required'],
            'amount'          => ['required', 'between:0,99.99'],
            'tax'             => ['required', 'between:0,99.99'],
            'expense'         => ['required', 'between:0,99.99'],
            'folio'           => ['required'],
            'notes'           => ['nullable', 'max:250'],
            'voucher'         => ['nullable']
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->ValidatorFormUpdate($request->all())->validate();

        $me = $request->user();

        $expense = Expense::findOrFail($id);
        $expense->user_id = $me['id'];
        $expense->date = $this->crateFromDate($request->date['year'], $request->date['month'], $request->date['day']);
        $expense->fill($request->except(['date', 'user_id']))->save();

        return $expense;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getByDate($from, $to)
    {
        $query = Expense::with(['service', 'provider']);

        if ($from != 'null' && $to != 'null') {
            $query->whereBetween('date', [$from, $to]);

        } else if ($from != 'null') {

            $query->whereDate('date', '>=', $from);

        } else if ($to != 'null') {

            $query->whereDate('date', '<=', $to);

        }

        return $query->get();
    } 
}
