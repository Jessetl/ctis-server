<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App;
use Session;
use App\User;
use App\Product;
use App\Service;

class ProductController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $me = $request->user();

        if ($me->position_id == 1) {
            $products = Product::latest()->get();
        }

        return $products;
    }

    /**
     * Return identifier.
     *
     * @return \Illuminate\Http\Response
     */
    public function getId()
    {
        $identifier = $this->generateId();

        return ['identifier' => $identifier];
    }

    /**
     * Generate a identifier unique.
     *
     * @return \Illuminate\Http\Response
     */
    private function generateId()
    {
        $identifier = strtoupper(str_random(5));

        $exists_service = Service::where('identifier', $identifier)->exists();
        $exists_product = Product::where('identifier', $identifier)->exists();

        if ($exists_product || $exists_service) {
            $this->generateId();
        }

        return $identifier;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormStore(array $data)
    {
        return Validator::make($data, [
            'identifier' => ['required', 'unique:products'],
            'account_id' => ['required', 'numeric'],
            'type'       => ['required'],
            'name'       => ['required'],
            'price'      => ['required', 'numeric'],
            'cost'       => ['required', 'numeric'],
            'high'       => ['nullable', 'numeric'],
            'width'      => ['nullable', 'numeric'],
            'height'     => ['nullable', 'numeric'],
            'quantity'   => ['required', 'numeric'],
            'weight'     => ['nullable', 'numeric']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->ValidatorFormStore($request->all())->validate();

        $me = $request->user();

        $product = new Product();
        $product->user_id = $me['id'];
        $product->fill($request->except(['user_id']))->save();

        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);

        return $product;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormUpdate(array $data)
    {
        return Validator::make($data, [
            'account_id' => ['required', 'numeric'],
            'type'       => ['required'],
            'name'       => ['required'],
            'price'      => ['required', 'numeric'],
            'cost'       => ['required', 'numeric'],
            'high'       => ['nullable', 'numeric'],
            'width'      => ['nullable', 'numeric'],
            'height'     => ['nullable', 'numeric'],
            'quantity'   => ['required', 'numeric'],
            'weight'     => ['nullable', 'numeric']
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->ValidatorFormUpdate($request->all())->validate();

        $me = $request->user();

        $product = Product::findOrFail($id);
        $product->user_id = $me['id'];
        $product->fill($request->except(['user_id', 'identifier']))->save();

        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    /**
     * Get products filtered for type service
     *
     * @param  int $type_id
     * @return \Illuminate\Http\Response
     */
    public function getByType($account, $type)
    {
        $products = Product::where('type', $type)->groupBy('name')->pluck('name')->toArray();

        return $products;
    }

    /**
     * Get products filtered for type service
     *
     * @param  int $type_id
     * @return \Illuminate\Http\Response
     */
    public function getByName($account, $name)
    {
        $products = Product::where('name')->get();

        return $products;
    }
}
