<?php

namespace App\Http\Controllers;

use App\RestCountry;

class RestCountryController extends Controller
{
    public function index()
    {
        return RestCountry::all();
    }
}
