<?php

namespace App\Http\Controllers;

use App\File;
use App\General;
use App\Http\Traits\ParseFormat;
use App\Http\Traits\PDFs;
use App\Invoice;
use App\Mail\EmailInvoice;
use App\Order;
use App\Payment;
use Illuminate\Http\Request;
use Mail;
use PDF;

define('CANCELED', 0);
define('PENDING', 1);
define('AUTHORIZED', 2);
define('PROCESS', 3);

class InvoiceController extends Controller
{
    use PDFs, ParseFormat;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the quotation resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getInvoices(Request $request)
    {
        $me = $request->user();

        $invoices = Invoice::with([

            'order',
            'order.file',
            'order.account',
            'order.customer',
            'order.addre.customer',
            'order.activities.activity',
            'order.activities' => function ($query) {
                $query->orderBy('work_date', 'ASC');
            },
            'order.assignments',
            'order.assignments.user',
            'order.assignments.activity',
            'order.assignments.user.position',

        ])->orderBy('id', 'DESC')->get();

        return $invoices;
    }

    /**
     * Ger an order by date.
     *
     * @return \Illuminate\Http\Response
     */
    public function getByDate($from, $to)
    {
        $query = Invoice::with([
            'order',
            'order.file',
            'order.account',
            'order.customer',
            'order.addre.customer',
            'order.assignaments.position',
        ]);

        if ($from != 'null' && $to != 'null') {
            $query->whereBetween('work_date', [$from, $to]);

        } else if ($from != 'null') {

            $query->whereDate('work_date', '>=', $from);

        } else if ($to != 'null') {

            $query->whereDate('work_date', '<=', $to);

        }

        return $query->get();
    }

    /**
     * Create PDF of an order.
     *
     * @return \Illuminate\Http\Response
     */
    public function fileInvoice(Request $request, $id)
    {
        $me = $request->user();

        $order = Order::with([
            'user',
            'file',
            'activity',
            'account',
            'details',
            'customer',
            'addre.customer',
            'details.product',
            'details.service',
        ])->findOrFail($id);

        $order->assignments = $order->assignments->unique('user_id');

        if ($order->activity->contains('pivot.activity_id', 1)) {

            $activity_delivery = $order->activity->where('pivot.activity_id', 1);
            $count = $activity_delivery->count();

            if ($count > 1) {

                $delivery = $activity_delivery->get();
                $order->activity_delivery = $this->parseFormat($delivery[0]->pivot->work_date) . ' - ' . $this->parseFormat($delivery[$count - 1]->pivot->work_date);

            } else {

                $order->activity_delivery = $this->parseFormat($activity_delivery->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 2)) {

            $activity_packaging_date = $order->activity->where('pivot.activity_id', 2);
            $count = $activity_packaging_date->count();

            if ($count > 1) {

                $packaging_date = $activity_packaging_date->get();
                $order->activity_packaging_date = $this->parseFormat($packaging_date[0]->pivot->work_date) . ' - ' . $this->parseFormat($packaging_date[$count - 1]->pivot->work_date);

            } else {

                $order->activity_packaging_date = $this->parseFormat($activity_packaging_date->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 3)) {

            $activity_unpack_range = $order->activity->where('pivot.activity_id', 3);
            $count = $activity_unpack_range->count();

            if ($count > 1) {

                $unpack_range = $activity_unpack_range->get();
                $order->activity_unpack_range = $this->parseFormat($unpack_range[0]->pivot->work_date) . ' - ' . $this->parseFormat($unpack_range[$count - 1]->pivot->work_date);

            } else {

                $order->activity_unpack_range = $this->parseFormat($activity_unpack_range->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 4)) {

            $activity_load_range = $order->activity->where('pivot.activity_id', 4);
            $count = $activity_load_range->count();

            if ($count > 1) {

                $load_range = $activity_load_range->get();
                $order->activity_load_range = $this->parseFormat($load_range[0]->pivot->work_date) . ' - ' . $this->parseFormat($load_range[$count - 1]->pivot->work_date);

            } else {

                $order->activity_load_range = $this->parseFormat($activity_load_range->first()->pivot->work_date);
            }
        }

        if (count($order->activity) > 0) {
            $order->services = $order->activity->unique('pivot.activity_id')->sortBy('pivot.id')->implode('name', ', ');
        }

        $path = $this->createFileInvoice($order);

        if ($find = $order->file()->where('type', 'invoice')->first()) {
            $file = File::findOrFail($find['id']);
        } else {
            $file = new File();
        }

        $file->order_id = $order['id'];
        $file->url_document = $path;
        $file->type = 'invoice';
        $file->save();

        $invoice = new Invoice();
        $invoice->order_id = $order['id'];
        $invoice->factoring = 0;
        $invoice->type = 'FC';
        $invoice->status = false;
        $invoice->user_id = $me['id'];
        $invoice->save();

        return $order;
    }

    public function sendEmail(Request $request)
    {
        $order = Order::with([
            'user',
            'file',
            'account',
            'details',
            'customer',
            'addre.customer',
            'details.product',
            'details.service',
        ])->findOrFail($request->Id);

        $file = $order->file[2];
        $order->file = $file;

        $email = $order->invoice_to ? $order->customer->email : $order->addre->customer->email;

        Mail::to($email)->send(new EmailInvoice($order));

        return $order;
    }

    public function factoring(Request $request, $id)
    {
        $general = General::findOrFail(1);
        $me = $request->user();

        $invoice = Invoice::with([
            'order',
        ])->findOrFail($id);

        $factoring = $general->factoring / 100;
        $total_factoring = $invoice->order->total * $factoring;
        $invoice->status = true;
        $invoice->type = 'FT';
        $invoice->factoring = $total_factoring;
        $invoice->total_factoring = $invoice->order->total - $total_factoring;
        $invoice->user_id = $me['id'];
        $invoice->save();

        $order = Order::with([
            'user',
            'file',
            'activity',
            'account',
            'customer',
            'details',
            'invoice',
            'addre.customer',
            'details.product',
            'details.service',
        ])->findOrFail($invoice['order_id']);

        $order->assignments = $order->assignments->unique('user_id');

        if ($order->activity->contains('pivot.activity_id', 1)) {

            $activity_delivery = $order->activity->where('pivot.activity_id', 1);
            $count = $activity_delivery->count();

            if ($count > 1) {

                $delivery = $activity_delivery->get();
                $order->activity_delivery = $this->parseFormat($delivery[0]->pivot->work_date) . ' - ' . $this->parseFormat($delivery[$count - 1]->pivot->work_date);

            } else {

                $order->activity_delivery = $this->parseFormat($activity_delivery->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 2)) {

            $activity_packaging_date = $order->activity->where('pivot.activity_id', 2);
            $count = $activity_packaging_date->count();

            if ($count > 1) {

                $packaging_date = $activity_packaging_date->get();
                $order->activity_packaging_date = $this->parseFormat($packaging_date[0]->pivot->work_date) . ' - ' . $this->parseFormat($packaging_date[$count - 1]->pivot->work_date);

            } else {

                $order->activity_packaging_date = $this->parseFormat($activity_packaging_date->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 3)) {

            $activity_unpack_range = $order->activity->where('pivot.activity_id', 3);
            $count = $activity_unpack_range->count();

            if ($count > 1) {

                $unpack_range = $activity_unpack_range->get();
                $order->activity_unpack_range = $this->parseFormat($unpack_range[0]->pivot->work_date) . ' - ' . $this->parseFormat($unpack_range[$count - 1]->pivot->work_date);

            } else {

                $order->activity_unpack_range = $this->parseFormat($activity_unpack_range->first()->pivot->work_date);
            }
        }

        if ($order->activity->contains('pivot.activity_id', 4)) {

            $activity_load_range = $order->activity->where('pivot.activity_id', 4);
            $count = $activity_load_range->count();

            if ($count > 1) {

                $load_range = $activity_load_range->get();
                $order->activity_load_range = $this->parseFormat($load_range[0]->pivot->work_date) . ' - ' . $this->parseFormat($load_range[$count - 1]->pivot->work_date);

            } else {

                $order->activity_load_range = $this->parseFormat($activity_load_range->first()->pivot->work_date);
            }
        }

        if (count($order->activity) > 0) {
            $order->services = $order->activity->unique('pivot.activity_id')->sortBy('pivot.id')->implode('name', ', ');
        }

        $path = $this->createFileFactoring($order);

        if ($find = $order->file()->where('type', 'factoring')->first()) {
            $file = File::findOrFail($find['id']);
        } else {
            $file = new File();
        }

        $file->order_id = $order['id'];
        $file->url_document = $path;
        $file->type = 'factoring';
        $file->save();

        $payment = new Payment();
        $payment->invoice_id = $invoice['id'];
        $payment->type = true;
        $payment->method_payment = 'T';
        $payment->amount = $order->total;
        $payment->user_id = $me['id'];
        $payment->save();

        return $order;
    }
}
