<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Module;
use App\User;

class ModulesController extends Controller
{

  /**
   * Create a new AuthController instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('jwt.auth');
  }

  public function getModules(Request $request)
  {
    $modules = Module::with([
      'items',
      'items.subitems'
    ])->get();

    return $modules;
  }

  public function getUserModules(Request $request)
  {
    $modules = User::with([
      'role' => function ($query) {
        $query->with(['modules', 'permissions']);
      }
    ])->where('id', auth()->user()->id)->firstOrFail();

    return $modules;
  }
}
