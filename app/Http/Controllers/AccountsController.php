<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use Mail;
use App\User;
use App\Account;
use Carbon\Carbon;
use App\Commission;
use App\Mail\EmailConfirmation;

class AccountsController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $accouts = Account::with('customers')->orderBy('id', 'DESC')->get();

        return $accouts;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormStore(array $data)
    {
        return Validator::make($data, [
            'name'              => ['required'],
            'address'           => ['required'],
            'phone'             => ['required', 'numeric', 'digits_between:7,10'],
            'email'             => ['required', 'string', 'email', 'unique:users'],
            'postal_code'       => ['required', 'numeric'],
            'country_alphacode' => ['required'],
            'state'             => ['required'],
            'city'              => ['required'],
            'trip_charge'       => ['required'],
            'min_amount'        => ['required'],
            'name_contact'      => ['required'],
            'email_contact'     => ['required', 'string', 'email'],
            'phone_contact'     => ['required', 'numeric', 'digits_between:7,10'],
            'value'             => ['nullable', 'numeric'],
            'user'              => ['nullable', 'alpha_dash', 'unique:users'],
            'password'          => ['nullable', 'string', 'min:6']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->ValidatorFormStore($request->all())->validate();

        $me = $request->user();
        $haveAccess = false;

        if (!empty($request['user']) or !empty($request['password'])) {

            $user = new User();
            $user->user_id = $me['id'];
            $user->account_id = $me['account_id'];
            $user->property = 'C';
            $user->password = bcrypt($request['password']);
            $user->satus = false;
            $user->encrypted_confirmation = str_random(25);
            $user->fill($request->except(['user_id', 'account_id', 'property', 'password', 'status', 'encrypted_confirmation']))->save();

            Mail::to($user->email)->send(new EmailConfirmation($user));

            $haveAccess = true;
        }

        $account = new Account();
        $account->user_id = $me['id'];
        $account->haveAccess = $haveAccess;
        $account->fill($request->except('haveAccess'))->save();

        if (!empty($request['value'])) {

            $commission = new Commission();
            $commission->account_id = $account['id'];
            $commission->user_id = $me['id'];
            $commission->fill($request->all())->save();
        }

        return $account;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = Account::with('commission')->findOrFail($id);
        $account->user = $account->haveAccess ? User::where('email', $account->email)->first() : null;

        return $account;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormUpdate(array $data, $id)
    {
        return Validator::make($data, [
            'name'              => ['required'],
            'address'           => ['required'],
            'phone'             => ['required', 'numeric', 'digits_between:7,10'],
            'email'             => ['required', 'email', Rule::unique('accounts', 'email')->ignore($id)],
            'postal_code'       => ['required', 'numeric'],
            'country_alphacode' => ['required'],
            'state'             => ['required'],
            'city'              => ['required'],
            'trip_charge'       => ['required'],
            'min_amount'        => ['required'],
            'name_contact'      => ['required'],
            'email_contact'     => ['required', 'string', 'email'],
            'phone_contact'     => ['required', 'numeric', 'digits_between:7,10'],
            'value'             => ['nullable', 'numeric']
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorFormUserUpdate(array $data, $id)
    {
        return Validator::make($data, [
            'user'     => ['required', 'alpha_dash', Rule::unique('users', 'user')->ignore($id)],
            'password' => ['nullable', 'string', 'min:6']
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->ValidatorFormUpdate($request->all(), $id)->validate();

        $me = $request->user();
        $account = Account::with('commission')->findOrFail($id);
        $userFromAccounts = $account->haveAccess ? User::where('email', $account->email)->first() : null;
        $haveAccess = $account->haveAccess;

        if ((!empty($request['user']) or !empty($request['password'])) and (!$account['haveAccess'])) {

            $user = new User();
            $user->user_id = $me['id'];
            $user->account_id = $me['account_id'];
            $user->property = 'C';
            $user->password = bcrypt($request['password']);
            $user->status = false;
            $user->encrypted_confirmation = str_random(25);
            $user->fill($request->except(['user_id', 'account_id', 'property', 'password', 'status', 'encrypted_confirmation']))->save();

            Mail::to($user->email)->send(new EmailConfirmation($user));

            $haveAccess = true;
        } elseif ((!empty($request['password'])) and ($account['haveAccess'])) {

            $this->validatorFormUserUpdate($request->all(), $userFromAccounts['id'])->validate();

            $user = $userFromAccounts;
            $user->user = $request['user'];
            $user->password = bcrypt($request['password']);
            $user->save();
        } elseif ((!empty($request['user'])) and ($account['haveAccess'])) {

            $this->validatorFormUserUpdate($request->all(), $userFromAccounts['id'])->validate();

            $user = $userFromAccounts;
            $user->user = $request['user'];
            $user->save();
        }

        if ((!$account->commission) and (!empty($request['value']))) {

            $commission = new Commission();
            $commission->movement_type = 'I';
            $commission->user_id = $me['id'];
            $commission->account_id = $account['id'];
            $commission->fill($request->except(['movement_type', 'user_id', 'account_id']))->save();
        } elseif (($account->commission) and ($request['value'] !== $account->commission['value'])) {

            $commission = $account->commission;
            $commission->movement_type = 'M';
            $commission->fill($request->all())->save();
        }

        $account->user_id = $me['id'];
        $account->haveAccess = $haveAccess;
        $account->fill($request->except(['user_id', 'haveAccess']))->save();

        return $account;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function ValidatorFormUpdateFromOrder(array $data)
    {
        return Validator::make($data, [
            'name'              => ['required'],
            'address'           => ['required'],
            'phone'             => ['required', 'numeric', 'digits_between:7,10'],
            'email'             => ['required', 'string', 'email', 'unique:users'],
            'postal_code'       => ['required', 'numeric'],
            'country_alphacode' => ['required'],
            'state'             => ['required'],
            'city'              => ['required']
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateFromOrder(Request $request, $id)
    {
        $this->ValidatorFormUpdateFromOrder($request->all())->validate();

        $me = $request->user();

        $account = Account::findOrFail($id);
        $account->user_id = $me['id'];
        $account->fill($request->except('user_id'))->save();

        $data = [
            'message' => trans('messages.update'),
            'account' => $account
        ];

        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeStatusAccount($id)
    {
        $account = Account::findOrFail($id);
        $account->status = (($account->status)) ? false : true;
        $account->save();

        return $account;
    }
}
