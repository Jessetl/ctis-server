<?php

namespace App\Http\Middleware;

use Closure;
/**
 * Class LocaleMiddleware.
 */
class LocalizationMiddleware
{
  public function handle($request, Closure $next)
  {
    // Check header request and determine localizaton
    $local = ($request->hasHeader('X-localization')) ? $request->header('X-localization') : 'es';
    // set laravel localization
    app()->setLocale($local);
    // continue request
    return $next($request);
  }
}