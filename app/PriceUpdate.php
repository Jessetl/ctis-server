<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceUpdate extends Model
{
    protected $table = "prices_update";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id', 'detail_id', 'after_price', 'before_price', 'reason', 'user_id', 'location'
    ];
}
