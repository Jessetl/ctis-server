<?php

namespace App;

use Laratrust\LaratrustRole;

class Role extends LaratrustRole
{
	protected $table = "roles";

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'display_name', 'description'
	];

		/**
	 * Obtain the module relationship
	 *
	 * @return mixed
	 */
	public function modules()
	{
		return $this->belongsToMany('App\Module', 'role_module', 'role_id', 'module_id')->withTimestamps();
	}

	/**
	 * Obtain the user module relationship
	 *
	 * @return mixed
	 */
	public function permissions()
	{
		return $this->belongsToMany('App\Permission', 'permission_role', 'role_id', 'permission_id')->withTimestamps();
	}
}
