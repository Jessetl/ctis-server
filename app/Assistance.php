<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assistance extends Model
{
    use SoftDeletes;

    protected $table = "assists";
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'order_id', 'user_id', 'date', 'status', 'haveTravelTime'
    ];

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function hours()
    {
        return $this->hasMany('App\Hour', 'assists_id');
    }
}
