<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    protected $table = "informations";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'country_alphacode', 'country_es', 
        'country_en', 'state', 'city', 'postal_code', 'phone', 'email'
    ];
}
