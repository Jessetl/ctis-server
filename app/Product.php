<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'identifier', 'account_id', 'type', 'name', 'price', 'cost', 'type_load',
        'quantity', 'width', 'height', 'high', 'weight', 'description', 'user_id', 'location'
    ];
}

