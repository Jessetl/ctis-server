<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Detail extends Model
{
    use SoftDeletes;

    protected $table = "details";
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'order_id', 'article_id', 'type', 'description', 'quantity', 'unitary_price', 'price',
       'load_type', 'height', 'high', 'width', 'measure_height', 'measure_high', 'measure_width',
       'foot_cubic', 'weight_vol', 'weight', 're_weight', 'insurance', 'taxation', 'subtotal', 'total', 'content'
    ];

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }

    public function product() 
    {
        return $this->belongsTo('App\Product', 'article_id');
    }

    public function service()
    {
        return $this->belongsTo('App\Service', 'article_id');
    }
}
