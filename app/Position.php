<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = "positions";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function users ()
    {
        return $this->hasMany('App\User', 'position_id', 'id');
    }

    public function pounds ()
    {
        return $this->hasOne('App\Pound', 'position_id', 'id');
    }
}
