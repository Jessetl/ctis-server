<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class General extends Model
{
    protected $table = 'generals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'percentage_trip', 'factoring', 'taxes', 'commission', 
        'card_payment', 'insurance', 'accound_id', 'user_id', 'location'
    ];
}
