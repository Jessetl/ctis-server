<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, LaratrustUserTrait, SoftDeletes;

    protected $table = "users";
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url', 'name', 'email', 'email_verified_at', 'address', 'phone', 'country_alphacode', 'country_es', 'country_en',
        'mobile', 'postal_code', 'latitude', 'longitude', 'user', 'password', 'state', 'city', 'position_id', 'property', 'account_id', 'inactive_from', 'language',
        'user_id', 'status', 'salaryActive', 'encrypted_confirmation',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Obtain the user's profile relationship
     *
     * @return mixed
     */
    public function modules()
    {
        return $this->belongsTo('App\Module', 'user_module', 'user_id', 'module_id');
    }

    /**
     * Obtain the user's permissions relationship
     *
     * @return mixed
     */
    public function role()
    {
        return $this->belongsToMany('App\Role', 'role_user', 'user_id', 'role_id');
    }

    /**
     * Obtain the user's job relationship
     *
     * @return mixed
     */
    public function position()
    {
        return $this->belongsTo('App\Position', 'position_id');
    }

    /**
     * Obtain the user's account relationship
     *
     * @return mixed
     */
    public function account()
    {
        return $this->belongsTo('App\Account', 'account_id');
    }

    /**
     * Obtain user assistance
     *
     * @return mixed
     */
    public function assists()
    {
        return $this->hasMany('App\Assistance', 'user_id', 'id');
    }

    /**
     * Obtain user assignments
     *
     * @return mixed
     */
    public function assignments()
    {
        return $this->hasMany('App\Assignment', 'user_id', 'id');
    }

    /**
     * Obtain user salary
     *
     * @return mixed
     */
    public function salary()
    {
        return $this->hasOne('App\Salary', 'user_id', 'id');
    }
}
