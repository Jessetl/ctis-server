<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

    protected $table = "customers";
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'phone', 'email', 'country_alphacode', 'country_es', 'country_en', 'state', 'city', 'postal_code', 'status', 'account_id', 'user_id', 'haveAccess'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function account()
    {
        return $this->belongsTo('App\Account', 'account_id');
    }

    public function address()
    {
        return $this->hasMany('App\Addre', 'customer_id');
    }
}
