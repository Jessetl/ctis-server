<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conditional extends Model
{
    use SoftDeletes;

    protected $table = "conditionals";
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pound_id', 'min', 'max', 'amount', 'user_id', 'location'
    ];
}
