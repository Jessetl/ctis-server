<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestCountry extends Model
{
    protected $primaryKey = 'alpha2Code';
    protected $keyType = 'string';

    public $incrementing = false;
}
