<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assignment extends Model
{
    use SoftDeletes;

    protected $table = "assignments";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id', 'order_id', 'activity_id', 'work_date', 'type_payment', 'work_begins', 'work_ends', 'assigned_by', 'status',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }

    public function activity()
    {
        return $this->belongsTo('App\Activity', 'activity_id');
    }
}
