<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = "invoices";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'factoring', 'total_factoring', 'paid', 'type', 'status', 'user_id', 'location'
    ];


    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id', 'id');
    }
}
