@component('mail::message')

Hola {{ $user->name }}, bienvenido a **CML Support Groups** 

No respondas a este correo electrónico, ya que no se verá. Para asistencia, vaya a https://cmlgroupapps.com/

Este mensaje ha sido enviado para validar su dirección de correo electrónico  {{ $user->email }}.

@component('mail::button', ['url' => \URL::route('verify', $user->encrypted_confirmation), 'color' => 'blue'])
CONFIRMAR MI CUENTA
@endcomponent

¡Gracias por usar CML Support Groups!<br>
{{ config('app.name') }}

@endcomponent
