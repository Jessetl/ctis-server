@component('mail::message')

Hola tienes una nueva notificación **CML Support Groups** 

No respondas a este correo electrónico, ya que no se verá. Para asistencia, vaya a https://cmlgroupapps.com/

Este mensaje ha sido enviado para una solicitud de cotización por  {{ $order->user->name }}.

@component('mail::button', ['url' => $order->file->url_document, 'color' => 'blue'])
DESCARGAR COTIZACIÓN
@endcomponent

¡Gracias por usar CML Support Groups!<br>
{{ config('app.name') }}

@endcomponent