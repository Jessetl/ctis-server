<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>QUOTE - {{ $order->identifier }}</title>

	<link rel="stylesheet" href="{{ asset('stylesheets/styles.css') }}" />
</head>

<body>
	<!-- header -->
	<header>
		<div>
			<div class="stylesheet-logo">
				<img src="{{ asset('img/logo.jpg') }}" class="img" width="200" />
			</div>
			<div class="stylesheet-letterhead">
				<div class="headletterhead">
					<h2 class="h2-head"> CML Exports Inc.</h2>
						<span class="font-address"> 112 Bartram Oaks Walk # 600877
							<br> Jacksonville FL 32259 Phone: (904) 460 2718
							<br> <a href="mailto:operations@cmlexports.com">operations@cmlexports.com</a>
						</span>
				</div>
			</div>
			<div class="stylesheet-type">
				<h2> QUOTE <p class="bg-ligthgray">
				<span class="stylesheet-folio"> {{ $order->identifier }} </span></p>
						<p class="text-center font-size-medium">
						@if( (!is_null($order->number_order)) )
						<br />
							<span class="font-weight-bold">Order No.</span>
							{{ $order->number_order }}
						@endif
						</p>
				</h2>
			</div>
		</div>
	</header>
	<!-- end header -->
	<!-- main -->
	<main>
	<div class="content-header">
    <table width="100%" border="1" cellspacing="0" cellpadding="0">
				<tr class="text-min sborder">
					<td class="text-uppercase" width="25%">Order No. {{ $order->number_order }}</th>
					<td class="text-uppercase" width="25%">GBL No. {{ $order->gbl }}</th>
					<td class="text-uppercase" width="25%">Ticket No. {{ $order->work_ticket}}</th>
					<td class="text-uppercase" width="25%">Ref. No. {{ $order->ref }}</th>
				</tr>
				<tr class="left">
					<td colSpan="2" width="50%"> <span class="font-weight-bold">Seller/Coord.:</span>
					{{ $order->user->name }}</td>
					<td colSpan="2" width="50%"> <span class="font-weight-bold">Issued on: </span>
					 {{ date("m/d/Y", strtotime($order->created_at)) }}</td>
				</tr>
				<thead class="bg-gray">
					<tr class="text-center" style="font-size: 13%px;">
						<th colSpan="2" width="50%">BILLTO</th>
						<th colSpan="2" width="50%">CUSTOMER</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colSpan="2" width="50%">
						<span class="font-weight-bold">Name:</span>
							<span class="left">{{ $order->account->name }} ( {{ $order->account_id }} )</span>
						</td>
						<td colSpan="2" width="50%">
						<span class="font-weight-bold">Name:</span>
							<span class="left">{{ !$order->invoice_to ? $order->addre->customer->name : $order->customer->name }}</span>
						</td>
					</tr>
					<tr>
						<td colSpan="2" width="50%">
							<span class="font-weight-bold">Address:</span>
							<span class="left">{{ $order->invoice_to ? $order->account->address : $order->addre->customer->address }}
                                <br /> {{ $order->invoice_to ? $order->account->city : $order->addre->customer->city }}, {{ $order->invoice_to ? $order->account->state : $order->addre->customer->state }} {{ $order->invoice_to ? $order->account->postal_code : $order->addre->customer->postal_code }} {{ $order->invoice_to ? $order->account->country_alphacode : $order->addre->customer->country_alphacode }}
                                <br /><strong>Phone:</strong> {{ $order->invoice_to ? $order->account->phone : $order->addre->customer->phone }}</span>
						</td>
						<td colSpan="2" width="50%">
							<span class="font-weight-bold">Address:</span>
							<span class="left">{{ !$order->invoice_to ? $order->addre->address : $order->customer->address }}
                                <br /> {{ !$order->invoice_to ? $order->addre->city : $order->customer->city }}, {{ !$order->invoice_to ? $order->addre->state : $order->customer->state }} {{ !$order->invoice_to ? $order->addre->postal_code : $order->customer->postal_code }}  {{ !$order->invoice_to ? $order->addre->country_alphacode : $order->customer->country_alphacode }}
                            <br /><strong>Phone:</strong> {{ !$order->invoice_to ? $order->addre->phone : $order->customer->phone }}</span>
						</td>
					</tr>
					@if( (!is_null($order->instructions_especials)) )
					<tr>
						<td colspan="4" width="100%">
							<span class="font-weight-bold">Indications:</span>
							<span class="left">{{ $order->instructions_especials }}</span>
						</td>
					</tr>
					@endif
				</tbody>
    </table>
			<table width="100%" border="1" cellspacing="0" cellpadding="0">
				<thead class="bg-gray">
					<tr class="text-center">
						<th colspan="5" style="font-size: 13px;">JOB INFORMATION</th>
					</tr>
				</thead>
				<tbody>
					<tr class="text-center font-weight-bold">
						<td width="12%"> Date </td>
						<td width="22%"> Pack Range </td>
						<td width="22%"> Load Range</td>
						<td width="22%"> Del. Range</td>
						<td width="22%"> Unpack Range</td>
					</tr>
					<tr class="text-center">
						<td><strong>Spread</strong></td>
						<td>
						@if (isset($order->activity_packaging_date))
							{{ $order->activity_packaging_date }}
						@endif
						</td>
						<td>
						@if (isset($order->activity_load_range))
							{{ $order->activity_load_range }}
						@endif
						</td>
						<td>
						@if (isset($order->activity_delivery))
							{{ $order->activity_delivery }}
						@endif
						</td>
						<td>
						@if (isset($order->activity_unpack_range))
							{{ $order->activity_unpack_range }}
						@endif
						</td>
					</tr>
					<tr class="text-center">
						<td><strong>Preferred</strong></td>
						<td>
						@if( (!is_null($order->packaging_date)) )
							{{ date("m/d/Y", strtotime($order->packaging_date)) }}
						@endif
						</td>

						<td>
						@if( (!is_null($order->collect_date)) )
							{{ date("m/d/Y", strtotime($order->collect_date)) }}
						@endif
						</td>
						<td>
						@if( (!is_null($order->delivery_date)) )
							{{ date("m/d/Y", strtotime($order->delivery_date)) }}
						@endif
						</td>
						<td>
						@if( (!is_null($order->unpacking_date)) )
							{{ date("m/d/Y", strtotime($order->unpacking_date)) }}
						@endif
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<span class="font-weight-bold">Services:</span>
							<span>{{ $order->services }}</span>
						</td>
						<td colspan="2">
							<span class="font-weight-bold">Equipment: {{ $order->equipment }}</span>
							<span> </span>
						</td>
					</tr>
				</tbody>
			</table>
	</div>
		<br />
		<div class="content-body">
			<table border="1" cellspacing="0" cellpadding="0">
				<thead class="bg-gray">
					<tr class="text-center" style="font-size: 13px">
						<th class="min-width">Line</th>
						<th class="max-width">Service / Description</th>
						<th class="min-width">Qty</th>
						<th class="medium-width">Ft3</th>
						<th class="medium-width">U. Price</th>
						<th class="width">Amount</th>
					</tr>
				</thead>
				<tbody>
					@foreach($order->details as $key => $detail)
					<tr>
						<td class="text-center">{{ $key + 1 }}</td>
						<td class="description">
							@if($detail->type == '2')

							@if($detail->service->crate || $detail->service->uncrate)

							{{ $detail->description }} Dims {{ "[" . $detail->height . "x" . $detail->high . "x" . $detail->width . "]" }}
							{{ $detail->measure_height . "x" . $detail->measure_high . "x" . $detail->measure_width }}
							Content: {{ $detail->content }}

							@else

							{{ $detail->description }}

							@endif

							@else

							{{ $detail->description }}

							@endif
						</td>
						<td class="text-center">
							{{ $detail->quantity }}
						</td>
						<td class="text-center">
							{{ number_format($detail->foot_cubic, 2, ".", ",") }}
						</td>
						<td class="text-right">
							{{ number_format($detail->unitary_price, 2, ".", ",") }}
						</td>
						<td class="text-right">
							{{ number_format($detail->price, 2, ".", ",") }}
						</td>
					</tr>
					@endforeach
					@if( (!is_null($order->adjustment)) )
					<tr>
						<td class="text-center">{{ count($order->details) + 1 }}</td>
						<td class="description"> Adjustment </td>
						<td class="text-center"> 1 </td>
						<td class="text-center"> 0,00 </td>
						<td class="text-right">{{ number_format($order->adjustment, 2, ".", ",") }}</td>
						<td class="text-right">{{ number_format($order->adjustment, 2, ".", ",") }}</td>
					</tr>
					@endif

					<tr style="page-break-after: always;">
						<td colspan="3" rowspan="{{ ($order->type_payment === 2 || $order->type_payment === 3) ? 6 : 5 }}" class="text-justify font-size-small">

								<strong>Important: </strong> Crating based upon: (1) Items with less than 3" depth use 6" depth crate with additional 4" for length and width. (2) Items with 3 1/2" to 6" depth use 8" depth crate and additional 4" to length and width.
								(3) Items more than 6" deep add 4" to length, width & depth.When estimating plywood or heat treated lumber crate, will add $2.00 per cubic feet.
								Minimum Service Charge per Invoice $ {{ number_format($order->account->min_amount, 2, ".", ",") }} * Amount showed as total apply only if the payment is made by check or cash.

						</td>
						<td colspan="2">SUBTOTAL</td>
						<td>$
							<span class="float-right">
								{{ number_format( (!empty($order->adjustment)) ? $order->subtotal - ($order->taxation + $order->insurance) * 1 : $order->subtotal, 2, ".", ",") }}
							</span>
						</td>
					</tr>
					<tr>
						<td colspan="2">INSURANCE</td>
						<td>$
							<span class="float-right">
								{{ number_format($order->insurance, 2, ".", ",") }}
							</span>
						</td>
					</tr>
					<tr>
						<td colspan="2">TAX</td>
						<td>$
							<span class="float-right">
								{{ number_format($order->taxation, 2, ".", ",") }}
							</span>
						</td>
					</tr>
					@if($order->type_payment === 2 || $order->type_payment === 3)
					<tr>
						<td colspan="2">COM. BY CARD</td>
						<td>$
							<span class="float-right">
								{{ number_format($order->com_payment, 2, ".", ",") }}
							</span>
						</td>
					</tr>
					@endif
					<tr>
						<td colspan="2">TRIP CHARGE</td>
						<td>$
							<span class="float-right">
								{{ number_format($order->account->trip_charge, 2, ".", ",") }}
							</span>
						</td>
					</tr>
					<tr>
						<td colspan="2">TOTAL</td>
						<td>$
							<span class="float-right">
								{{ number_format($order->total, 2, ".", ",") }}
							</span>
						</td>
					</tr>
				</tbody>
			</table>
			<br />
			<div class="content-footer" style="font-size:13px;">
				<strong>CUSTOMER</strong>: {{ !$order->invoice_to ? $order->addre->customer->name : $order->customer->name }}
				<hr>
				<p class="min-font text-justify">
				I hereby acknowledge the above work was performed satisfactorily and without damage, any exceptions must be noted next to each line item. By signing this document, I agree to pay the amount showed in this QUOTE / INVOICE in full to <strong>Crating Third Party & Installs Services </strong> a CML Company.</p>
			</div>
			<br />
			<div class="text-center">
				<p>
					<table>
						<thead>
							<tr>
								<td width="31%"> </td>
								<td width="3.5%"></td>
								<td width="31%"> </td>
								<td width="3.5%"></td>
								<td width="31%"> </td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="font-weight-light border-black">PRINT NAME</td>
								<td></td>
								<td class="font-weight-light border-black">SIGNATURE</td>
								<td></td>
								<td class="font-weight-light border-black">DATE</td>
							</tr>
						</tbody>
					<table>
				</p>
			</div>
		</div>
	</main>
	<script type="text/php">
		if (isset($pdf)) {
			$text = "Page {PAGE_NUM} / {PAGE_COUNT}";
			$size = 10;
			$font = $fontMetrics->getFont("Verdana");
			$width = $fontMetrics->get_text_width($text, $font, $size) / 2;
			$x = ($pdf->get_width() - $width) / 2;
			$y = $pdf->get_height() - 35;
			$pdf->page_text($x, $y, $text, $font, $size);
		}
	</script>
</body>

</html>
