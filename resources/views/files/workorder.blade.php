<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>WORKORDER - {{ $order->identifier }}</title>

	<link rel="stylesheet" href="{{ asset('stylesheets/styles.css') }}" />
</head>

<body>
	<!-- header -->
	<header>
		<div>
			<div class="stylesheet-logo">
				<img src="{{ asset('img/logo.jpg') }}" class="img" width="200" />
			</div>
			<div class="stylesheet-letterhead">
				<div class="headletterhead">
                <h2 class="h2-head"> CML Exports Inc.</h2>
						<span class="font-address"> 112 Bartram Oaks Walk # 600877
							<br> Jacksonville FL 32259 Phone: (904) 460 2718
							<br> <a href="mailto:operations@cmlexports.com">operations@cmlexports.com</a>
						</span>
				</div>
			</div>
			<div class="stylesheet-type">
				<h2> WORK ORDER <p class="bg-ligthgray">
						<span class="stylesheet-folio"> {{ $order->identifier }} </span></p>
				</h2>
			</div>
		</div>
	</header>
	<!-- end header -->
	<!-- main -->
	<main>
		<div class="content-header">
			<table width="100%" border="1" cellspacing="0" cellpadding="0">
				<tr class="text-min sborder">
					<td class="text-uppercase" width="25%">Order No. {{ $order->number_order }}</th>
					<td class="text-uppercase" width="25%">GBL No. {{ $order->gbl }}</th>
					<td class="text-uppercase" width="25%">Ticket No. {{ $order->work_ticket}}</th>
					<td class="text-uppercase" width="25%">Ref. No. {{ $order->ref }}</th>
				</tr>
				<tr class="left">
					<td colSpan="2" width="50%"> <span class="font-weight-bold">Seller/Coord.:</span>
					{{ $order->user->name }}</td>
					<td colSpan="2" width="50%"> <span class="font-weight-bold">Issued on: </span>
					 {{ date("m/d/Y", strtotime($order->created_at)) }}</td>
				</tr>
				<thead class="bg-gray">
					<tr class="text-center" style="font-size: 13%px;">
						<th colSpan="2" width="50%">BILLTO</th>
						<th colSpan="2" width="50%">CUSTOMER</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colSpan="2" width="50%">
						<span class="font-weight-bold">Name:</span>
							<span class="left">{{ $order->account->name }} ( {{ $order->account_id }} )</span>
						</td>
						<td colSpan="2" width="50%">
						<span class="font-weight-bold">Name:</span>
							<span class="left">{{ !$order->invoice_to ? $order->addre->customer->name : $order->customer->name }}</span>
						</td>
					</tr>
					<tr>
						<td colSpan="2" width="50%">
							<span class="font-weight-bold">Address:</span>
							<span class="left">{{ $order->invoice_to ? $order->account->address : $order->addre->customer->address }} <br /> {{ $order->invoice_to ? $order->account->city : $order->addre->customer->city }}, {{ $order->invoice_to ? $order->account->state : $order->addre->customer->state }} {{ $order->invoice_to ? $order->account->postal_code : $order->addre->customer->postal_code }} &nbsp;&nbsp;&nbsp;<strong>Phone:</strong> {{ $order->invoice_to ? $order->account->phone : $order->addre->customer->phone }}</span>
						</td>
						<td colSpan="2" width="50%">
							<span class="font-weight-bold">Address:</span>
							<span class="left">{{ !$order->invoice_to ? $order->addre->address : $order->customer->address }} <br /> {{ !$order->invoice_to ? $order->addre->city : $order->customer->city }}, {{ !$order->invoice_to ? $order->addre->state : $order->customer->state }} {{ !$order->invoice_to ? $order->addre->postal_code : $order->customer->postal_code }} &nbsp;&nbsp;&nbsp;<strong>Phone:</strong> {{ !$order->invoice_to ? $order->addre->phone : $order->customer->phone }}</span>
						</td>
					</tr>
					@if( (!is_null($order->instructions_especials)) )
					<tr>
						<td colspan="4" width="100%">
							<span class="font-weight-bold">Indications:</span>
							<span class="left">{{ $order->instructions_especials }}</span>
						</td>
					</tr>
					@endif
				</tbody>
			</table>

			<table width="100%" border="1" cellspacing="0" cellpadding="0">
				<thead class="bg-gray">
					<tr class="text-center">
						<th colspan="5" width="100%" style="font-size: 13px;">JOB INFORMATION</th>
					</tr>
				</thead>
				<tbody>
					<tr class="text-center font-weight-bold">
						<td width="12%"> Date </td>
						<td width="22%"> Pack Range </td>
						<td width="22%"> Load Range</td>
						<td width="22%"> Del. Range</td>
						<td width="22%"> Unpack Range</td>
					</tr>
					<tr class="text-center">
					<td><strong>Spread</strong></td>
						<td>
						@if (isset($order->activity_packaging_date))
							{{ $order->activity_packaging_date }}
						@endif
						</td>
						<td>
						@if (isset($order->activity_load_range))
							{{ $order->activity_load_range }}
						@endif
						</td>
						<td>
						@if (isset($order->activity_delivery))
							{{ $order->activity_delivery }}
						@endif
						</td>
						<td>
						@if (isset($order->activity_unpack_range))
							{{ $order->activity_unpack_range }}
						@endif
						</td>
					</tr>
					<tr class="text-center">
						<td><strong>Preferred</strong></td>
						<td>
						@if( (!is_null($order->packaging_date)) )
							{{ date("m/d/Y", strtotime($order->packaging_date)) }}
						@endif
						</td>

						<td>
						@if( (!is_null($order->collect_date)) )
							{{ date("m/d/Y", strtotime($order->collect_date)) }}
						@endif
						</td>
						<td>
						@if( (!is_null($order->delivery_date)) )
							{{ date("m/d/Y", strtotime($order->delivery_date)) }}
						@endif
						</td>
						<td>
						@if( (!is_null($order->unpacking_date)) )
							{{ date("m/d/Y", strtotime($order->unpacking_date)) }}
						@endif
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<span class="font-weight-bold">Services:</span>
							<span>{{ $order->services }}</span>
						</td>
						<td colspan="2">
							<span class="font-weight-bold">Equipment: {{ $order->equipment }}</span>
							<span> </span>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="content-body">
			<table width="100%" border="1" cellspacing="0" cellpadding="0">
				<thead class="bg-gray">
					<tr class="text-center" style="font-size: 13px">
						<th class="min-width">Line</th>
						<th class="max-width">Service / Description</th>
						<th class="min-width">Qty</th>
						<th class="medium-width">Ft3</th>
					</tr>
				</thead>
				<tbody>
					@foreach($order->details as $key => $detail)
					<tr>
						<td class="text-center">{{ $key + 1 }}</td>
						<td class="description">
							@if($detail->type == '2')

							@if($detail->service->crate || $detail->service->uncrate)

							{{ $detail->description }} Dims {{ "[" . $detail->height . "x" . $detail->high . "x" . $detail->width . "]" }}
							{{ $detail->measure_height . "x" . $detail->measure_high . "x" . $detail->measure_width }}
							Content: {{ $detail->content }}

							@else

							{{ $detail->description }}

							@endif

							@else

							{{ $detail->description }}

							@endif
						</td>
						<td class="text-center">
							{{ $detail->quantity }}
						</td>
						<td class="text-center">
							{{ number_format($detail->foot_cubic, 2, ".", ",") }}
						</td>
					</tr>
					@endforeach
					@if( (!is_null($order->adjustment)) )
					<tr>
						<td class="text-center">{{ count($order->details) + 1 }}</td>
						<td class="text-center"> Adjustment for minimum billing amount </td>
						<td class="text-center"> 1 </td>
						<td class="text-center"> 0.00 </td>
					</tr>
					@endif
				</tbody>
			</table>
			<br />
			@if(count($order->assignments) > 0)
			<table width="100%" border="1" cellspacing="0" cellpadding="0">
				<thead class="bg-gray">
					<tr class="min-font text-center">
					<!-- Reemplazar texto No. de Empleados por el total de empleados asignados -->
						<th width="35%">Crew &nbsp; &nbsp; &nbsp;Total ({{ count($order->assignments) }}) </th>
						<th width="13%">Sched. Arrival</th>
						<th width="13%">Sched. Dept.</th>
						<th width="13%">Arrival</th>
						<th width="13%">Break</th>
						<th width="13%">Departure</th>
					</tr>
				</thead>
				<tbody>
				<!-- esta informacion debe de crear una linea por cada usuario asignado -->
				@foreach($order->assignments as $key => $assignment)
					<tr class="min-font">
						<td>{{ $assignment->user->name }} ({{ $assignment->user->position->name }}) </td>
						<td>{{ $order->work_begins }}</td>
						<td>{{ $order->work_ends }}</td>
						<td>{{ $assignment->work_begins }}</td>
						<td> </td>
						<td>{{ $assignment->work_ends }}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
			@endif
			<div class="text-center">
				<p class="font-weight-light">
				 I confirm that the requested services and products from Crating Third Party & Installs Services. Have been completed to my satisfaction and a walk-through has been done and no property damages where found.
					<p>
						<br>
					<table>
						<thead>
							<tr>
								<td width="31%" class="signature">
									<span>{{ $order->signatory }}</span>
								</td>
								<td width="3.5%" class="signature"></td>
								<td width="31%" class="signature">
								@if( !empty($order->signing) )
									<img src="{{ $order->signing }}" width="200" height="50" style="position: absolute; margin: -18px 25px auto;">
								@endif
								</td>
								<td width="3.5%" class="signature"></td>
								<td width="31%" class="signature">
								<span>
									@if( (!is_null($order->date_signatory)) )
										{{ date("m/d/Y", strtotime($order->date_signatory)) }}
									@endif
								</span>
								</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="font-weight-light border-black">PRINT NAME</td>
								<td></td>
								<td class="font-weight-light border-black">SIGNATURE</td>
								<td></td>
								<td class="font-weight-light border-black">DATE</td>
							</tr>
						</tbody>
					<table>
					</p>
				</p>
			</div>
		</div>
	</main>
	<script type="text/php">
		if (isset($pdf)) {
			$text = "Page {PAGE_NUM} / {PAGE_COUNT}";
			$size = 10;
			$font = $fontMetrics->getFont("Verdana");
			$width = $fontMetrics->get_text_width($text, $font, $size) / 2;
			$x = ($pdf->get_width() - $width) / 2;
			$y = $pdf->get_height() - 35;
			$pdf->page_text($x, $y, $text, $font, $size);
	}
	</script>
</body>

</html>
