<?php

use Illuminate\Http\Request;

Route::post('login', 'AuthController@login');
Route::post('refresh', 'AuthController@refresh');
