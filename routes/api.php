<?php

use Illuminate\Http\Request;

Route::namespace('Auth')->group(function ($router) {

    require __DIR__ . '/guest.php';

    Route::group(['middleware' => 'api'], function () {
        // Auth
        Route::post('logout', 'AuthController@logout');
        Route::post('me', 'AuthController@me');
    });
});

Route::namespace('Landing')->group(function ($router) {
    Route::get('onlyService/{Id}', 'LandingController@getOnlyService');
    Route::get('onlyProduct/{Id}', 'LandingController@getOnlyProduct');
    //
    Route::post('account', 'LandingController@getAccount');
    Route::post('allActivities', 'LandingController@getActivities');
    Route::post('allTypeServices', 'LandingController@getTypeServices');
    Route::post('allTypeProducts', 'LandingController@getTypeProducts');
    Route::post('allGenerals', 'LandingController@getGenerals');
    Route::post('onlyTerm', 'LandingController@getOnlyTerm');
    //
    Route::post('store/quotations', 'LandingController@storeQuotations');
    Route::post('store/details/{Id}', 'LandingController@storeDetails');
    Route::post('store/file', 'LandingController@storeFile');
    //
    Route::post('valid/customer', 'ValidLandingController@formCustomer');
    Route::post('valid/quote', 'ValidLandingController@formQuote');
    Route::post('valid/detail', 'ValidLandingController@formDetail');
});

Route::namespace('Modules')->group(function ($router) {
    // Permissions
    Route::post('modules', 'ModulesController@getModules');
    Route::post('modules/user', 'ModulesController@getUserModules');
});

Route::namespace('Activity')->group(function ($router) {
    // Activities
    Route::get('activities', 'ActivitiesController@getAll');
});

Route::namespace('Generals')->group(function ($router) {

    Route::group(['middleware' => 'api'], function () {
        // Positions
        Route::get('generals/positions', 'PositionsController@getPositionsWithConditionals');
        Route::put('generals/{id}/conditionals', 'PoundsController@updatePoundWithConditionals');
        // Generals
        Route::resource('generals', 'GeneralsController');
    });
});

Route::namespace('Users')->group(function ($router) {

    Route::group(['middleware' => 'api'], function () {
        //Users
        Route::get('positions/employee', 'PositionsController@getPositionsWithUsersEmployee');
        // Assignments
        Route::get('assignments', 'AssignmentsController@getUsersWithSalary');
        Route::get('assignments/salary/{id}', 'AssignmentsController@getSalaryWithUserEmployee');
        Route::post('assignments', 'AssignmentsController@storeUserSalary');
        Route::put('assignments/{id}', 'AssignmentsController@updateUserSalary');
    });
});

Route::namespace('Assists')->group(function ($router) {

    Route::group(['middleware' => 'api'], function () {
        // Assists
        Route::get('assists/users', 'UsersController@index');
        Route::get('assists/positions/users', 'PositionsController@getPositionsWithUsers');
        Route::get('assists/assignments/{id}/user', 'AssignmentsController@getUserAssignments');
        Route::get('assists/orders', 'OrdersController@getOrders');
        Route::get('assists/order/{id}', 'AssistsController@assists');
        Route::post('assists/hours', 'AssistsController@assistsHours');

        // Assists Resource
        Route::resource('assists', 'AssistsController');
    });
});

Route::namespace('Orders')->group(function ($router) {

    Route::group(['middleware' => 'api'], function () {
        // Assignments from Order
        Route::get('assignments/activities', 'AssignmentsController@getAssignmentsWithActivities');
        Route::get('assignments/order', 'AssignmentsController@getOrderWithAssignments');
        Route::get('assignments/movements', 'AssignmentsController@getAssignmentsWithUser');
        Route::get('assignments/{id}/authorize', 'AssignmentsController@authorizeAssignment');
        Route::get('assignments/{from}/{to}/movements/get-by-date', 'AssignmentsController@getByDate');
        Route::post('assignments/order', 'AssignmentsController@assignUser');
        Route::post('assignments/movements', 'AssignmentsController@assingTimesToAssignment');
        Route::delete('assignments/{id}/delete', 'AssignmentsController@deleteAssignedUser');
        // Positions
        Route::get('positions/employees', 'PositionsController@getPositionsWithSalaryActive');
        // Quotations
        Route::get('quotations', 'QuotationsController@getQuotations');
        Route::post('quotations/send', 'QuotationsController@sendEmail');
        Route::get('quotations/{id}/authorize', 'QuotationsController@authorizeQuote');
        Route::get('quotations/{from}/{to}/get-by-date', 'QuotationsController@getByDate');
        // WorkOrders
        Route::get('workorders', 'WorkOrderController@getWorkOrders');
        Route::post('workorders/send', 'WorkOrderController@sendEmail');
        Route::post('workorders/{id}/sign', 'WorkOrderController@signWorkOrder');
        Route::post('workorders/{id}/cancel', 'WorkOrderController@canceled');
        Route::get('workorders', 'WorkOrderController@getWorkOrders');
        Route::get('workorders/{id}/authorize', 'WorkOrderController@authorizeWorkOrder');
        Route::get('workorders/{from}/{to}/get-by-date', 'WorkOrderController@getByDate');
    });
});

Route::namespace('Payrolls')->group(function ($router) {

    Route::group(['middleware' => 'api'], function () {
        // Payrolls
        Route::post('payrolls/users-assists', 'UsersController@getAssistsByUser');
        Route::post('payrolls/validation', 'PayrollsController@payrollValidation');
        // Concepts
        Route::get('payrolls/concepts', 'ConceptsController@getConcepts');
        // Payroll Resources
        Route::resource('payrolls', 'PayrollsController', ['except' => ['create']]);
    });
});

//------------------------------------------------------------------------------------------

Route::group(['middleware' => 'api'], function () {
    // From orders
    Route::put('accounts/{id}/update-from-order', 'AccountsController@updateFromOrder');
    //--------------------------------------
    Route::post('customers/{id}/store-from-order', 'CustomersController@storeCustomerFromOrder');
    Route::put('customers/{id}/update-customer-from-order', 'CustomersController@updateCustomerFromOrder');
    Route::put('customers/{id}/update-address-from-order', 'CustomersController@updateAddressFromOrder');
    //--------------------------------------
    Route::get('invoices', 'InvoiceController@getInvoices');
    Route::post('invoices/send', 'InvoiceController@sendEmail');
    Route::get('invoices/{id}', 'InvoiceController@fileInvoice');
    Route::get('invoices/{id}/factoring', 'InvoiceController@factoring');
    Route::get('invoices/{from}/{to}/get-by-date', 'InvoiceController@getByDate');
    //--------------------------------------
    //Route::post('orders/assing', 'OrderController@assing');
    Route::post('orders/authorize', 'OrderController@authorized');
    Route::put('orders/customer', 'OrderController@createCustomer');
    Route::put('orders/{id}/account', 'OrderController@updateAccount');
    Route::put('orders/{id}/customer', 'OrderController@updateCustomer');
    Route::put('orders/customer', 'OrderController@createCustomer');
    Route::get('orders/activated', 'OrderController@activated');
    Route::get('orders/{id}/cancel', 'OrderController@canceled');
    Route::get('orders/{from}/{to}/get-by-date', 'OrderController@getByDate');
    //--------------------------------------
    Route::put('details/{id}', 'DetailsController@update');
    //--------------------------------------
    Route::put('documents/{id}/file-quote', 'DetailsController@update');
    Route::get('documents/{id}/update-file', 'DocumentsController@updateFile');
    //--------------------------------------
    // Generals  GET
    Route::get('services/get-id', 'ServiceController@getId');
    Route::get('products/get-id', 'ProductController@getId');
    Route::get('services/{account}/{type}/get-by-type', 'ServiceController@getByType');
    Route::get('services/{account}/{name}/get-by-name', 'ServiceController@getByName');
    Route::get('products/{account}/{type}/get-by-type', 'ProductController@getByType');
    Route::get('products/{account}/{name}/get-by-name', 'ProductController@getByName');
    Route::get('services/identifier', 'ServiceController@getId');
    Route::get('products/identifier', 'ProductController@getId');
    Route::get('documents/{id}/file-quote', 'DocumentsController@fileQuote');
    // Generals POST
    Route::post('validate/orders', 'ValidationsController@validateOrder');
    Route::post('validate/details', 'ValidationsController@validateDetail');
    Route::post('validate/price', 'ValidationsController@validatePrice');
    Route::post('details/{id}/remove-articles', 'DetailsController@destroy');
    Route::post('details/{id}', 'DetailsController@store');
    Route::get('expenses/{from}/{to}/get-by-date', 'ExpensesController@getByDate');
    Route::post('expenses/{id}/file', 'ExpensesController@uploadFile');
    //--------------------------------------
    // Status
    Route::get('users/{id}/change-status', 'UsersController@changeStatusUser');
    Route::get('accounts/{id}/change-status', 'AccountsController@changeStatusAccount');
    Route::get('customers/{id}/change-status', 'CustomersController@changeStatusCustomer');
    Route::get('providers/{id}/change-status', 'ProvidersController@changeStatusProvider');
    // Routes Customers
    Route::post('customers/{id}/address', 'CustomersController@address');
    Route::get('customers/{account}/{name}/get-by-name', 'CustomersController@getByName');
    Route::get('customers/{id}/get-addre', 'CustomersController@getAddre');
    Route::get('customers/{id}/get-address', 'CustomersController@getAddress');
    Route::get('users/{position_id}/position', 'UsersController@getByPosition');

    Route::get('services/{id}/providers', 'ProvidersController@getProviders');
    Route::get('{account_id}/type-services', 'TypeServicesController@getTypeServices');
    Route::get('{account_id}/type-products', 'TypeServicesController@getTypeProducts');
    Route::get('terms/{width}', 'TermsController@getIncraseWidth');

    Route::resources(['informations' => 'InformationController', 'services' => 'ServiceController', 'products' => 'ProductController', 'orders' => 'OrderController']);
    Route::resources(['activities' => 'ActivitiesController', 'accounts' => 'AccountsController', 'users' => 'UsersController', 'providers' => 'ProvidersController', 'expenses' => 'ExpensesController', 'customers' => 'CustomersController']);
    Route::resource('type/services', 'TypeServicesController', ['only' => ['index', 'type']]);
    //Route::resource('assignments', 'AssignmentsController', ['except' => ['store', 'show', 'destroy']]);
    Route::resource('positions', 'PositionsController', ['only' => ['index']]);
});

Route::group(['middleware' => 'api'], function () {
    Route::get('restcountries', 'RestCountryController@index');
});
