<?php

Route::get('verify-email/{string}', 'EmailVerifyController@verify')->name('verify');
Route::get('quote/{id}', 'OrderController@viewFile');
//Route::get('user', 'AuthController@userIsLogged');

Route::get('/', function () {
	return view('welcome');
});
